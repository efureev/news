module.exports =  {
	css_front: {
		files: [
			'<%= dirs.front_less %>/*.less',
		],
		tasks: ['less:dev_front']
	},

	css_back: {
		files: [
			'<%= dirs.back_less %>/*.less'
		],
		tasks: ['less:dev_back']
	},
	front_js: {
		files: [
			'<%= dirs.front_js_app %>/*.js'
		],
		tasks: ['concat:front_app']
	},
	back_js: {
		files: [
			'<%= dirs.back_js_app %>/*.js'
		],
		tasks: ['concat:back_app']
	}
};