module.exports = {
	front_dist: {
		src: '<%= scripts.front.libs %>',
		dest: '<%= dirs.front_assets_js %>/libs.min.js'
	},
	front_app: {
		src: '<%= scripts.front.app %>',
		dest: '<%= dirs.front_assets_js %>/app.min.js'
	},
	front_extra: {
		src: '<%= scripts.front.extra %>',
		dest: '<%= dirs.front_assets_js %>/extra.min.js'
	},

	back_dist: {
		src: '<%= scripts.front.libs %>',
		dest: '<%= dirs.back_assets_js %>/libs.min.js'
	},
	back_app: {
		src: '<%= scripts.front.app %>',
		dest: '<%= dirs.back_assets_js %>/app.min.js'
	},
	back_extra: {
		src: '<%= scripts.front.extra %>',
		dest: '<%= dirs.back_assets_js %>/extra.min.js'
	}
};