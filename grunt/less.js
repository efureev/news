module.exports =  {
	dev_front: {
		src    : [
			"<%= dirs.vendor %>/bower/bootstrap/dist/css/bootstrap.css",
			"<%= dirs.vendor %>/bower/bootstrap/dist/css/bootstrap-theme.css",
			'<%= dirs.front_less %>/styles.less'
		],
		dest   : "<%= dirs.front_assets_css %>/styles.css"
	},
	dev_front_adm: {
		src    : [
			'frontend/modules/adm/assets/less/styles.less'
		],
		dest   : "<%= dirs.front_assets_css %>/adm.css"
	},
	dev_back: {
		src    : [
			"<%= dirs.vendor %>/bower/bootstrap/dist/css/bootstrap.css",
			"<%= dirs.vendor %>/bower/bootstrap/dist/css/bootstrap-theme.css",
			'<%= dirs.back_less %>/styles.less'
		],
		dest   : "<%= dirs.back_assets_css %>/styles.css"
	},

	prod_front: {
		options: {
			compress	: true,
			cleancss	: true

		},
		src    : [
			"<%= dirs.vendor %>/bower/bootstrap/dist/css/bootstrap.css",
			"<%= dirs.vendor %>/bower/bootstrap/dist/css/bootstrap-theme.css",
			'<%= dirs.front_less %>/styles.less'
		],
		dest   : "<%= dirs.front_assets_css %>/styles.css"
	},
	prod_back: {
		options: {
			compress	: true,
			cleancss	: true

		},
		src    : [
			"<%= dirs.vendor %>/bower/bootstrap/dist/css/bootstrap.css",
			"<%= dirs.vendor %>/bower/bootstrap/dist/css/bootstrap-theme.css",
			'<%= dirs.back_less %>/styles.less'
		],
		dest   : "<%= dirs.back_assets_css %>/styles.css"
	}
};