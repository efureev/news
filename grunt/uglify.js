module.exports = {
	libs: {
		options: {
			sourceMap: true
		},
		src: '<%= scripts.front.libs %>',
		dest: '<%= dirs.front_assets_js %>/libs.min.js'
	},
	app: {
		options: {
			sourceMap: true
		},
		src: '<%= scripts.front.app %>',
		dest: '<%= dirs.front_assets_js %>/app.min.js'
	},
	extra: {
		options: {
			sourceMap: true
		},
		src: '<%= scripts.front.extra %>',
		dest: '<%= dirs.front_assets_js %>/extra.min.js'
	},

	all_front: {
		options: {
			sourceMap: false
		},
		src: [
			'<%= scripts.front.extra %>',
			'<%= scripts.front.libs %>',
			'<%= scripts.front.app %>'
		],
		dest: '<%= dirs.front_assets_js %>/js.min.js'
	},
	all_back: {
		options: {
			sourceMap: false
		},
		src: [
			'<%= scripts.back.extra %>',
			'<%= scripts.back.libs %>',
			'<%= scripts.back.app %>'
		],
		dest: '<%= dirs.back_assets_js %>/js.min.js'
	}
};