module.exports =  {
	images: {
		nonull: true,
		expand: true,
		cwd: '<%= dirs.front_images %>',
		src: [ '**' ],
		dest: '<%= dirs.front_assets_images %>'
	},
	fonts_back: {
		nonull: true,
		expand: true,
		cwd: '<%= dirs.front_fonts %>',
		src: [ '**' ],
		dest: '<%= dirs.back_assets_fonts %>'
	},
	fonts_front: {
		nonull: true,
		expand: true,
		cwd: '<%= dirs.front_fonts %>',
		src: [ '**' ],
		dest: '<%= dirs.front_assets_fonts %>'
	},
	img_front: {
		nonull: true,
		expand: true,
		cwd: '<%= dirs.front_images %>',
		src: [ '**' ],
		dest: '<%= dirs.front_assets_images %>'
	},

	back_fonts_bs: {
		nonull: true,
		expand: true,
		cwd: "<%= dirs.vendor %>/bower/bootstrap/dist/fonts",
		src: [ '**' ],
		dest: '<%= dirs.back_assets_fonts %>'
	},

	front_fonts_bs: {
		nonull: true,
		expand: true,
		cwd: "<%= dirs.vendor %>/bower/bootstrap/dist/fonts",
		src: [ '**' ],
		dest: '<%= dirs.front_assets_fonts %>'
	},

	back_lib_ext_js: {
		nonull: true,
		expand: true,
		cwd: "<%= dirs.vendor %>",
		flatten: true,
		src: [
			'/eonasdan/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js'
		],
		dest: '<%= dirs.back_js_ext_js %>'
	},
	back_lib_ext_css: {
		nonull: true,
		expand: true,
		cwd: "<%= dirs.vendor %>",
		flatten: true,
		src: [
			'/eonasdan/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'
		],
		dest: '<%= dirs.back_js_ext_css %>'
	}

};