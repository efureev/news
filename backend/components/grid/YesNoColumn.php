<?php

namespace backend\components\grid;

use yii\grid\DataColumn;

/**
 * YesNoColumn отображает колонки Да-Нет
 *
 * ```php
 * 'columns' => [
 *     // ...
 *     [
 *         'class' => 'yii\grid\YesNoColumn'
 *     ],
 * ]
 * ```
 *
 * @author feugene.org
 */
class YesNoColumn extends DataColumn
{
	/**
	 * @var string label for the true value. Defaults to `Active`.
	 */
	public $trueLabel;
	/**
	 * @var string label for the false value. Defaults to `Inactive`.
	 */
	public $falseLabel;

	/**
	 * Null показывать как FALSE
	 * @var bool
	 */
	public $showNullAsFalse = true;

	public function init()
	{
		if (empty($this->trueLabel)) {
			$this->trueLabel = 'Да';
		}
		if (empty($this->falseLabel)) {
			$this->falseLabel = 'Нет';
		}
		$this->filter = [true => $this->trueLabel, false => $this->falseLabel];

		parent::init();
	}

	/**
	 * @inheritdoc
	 */
	public function getDataCellValue($model, $key, $index)
	{
		$value = parent::getDataCellValue($model, $key, $index);
		if ($value !== null) {
			return $value ? $this->trueLabel : $this->falseLabel;
		}
		return $this->showNullAsFalse ? $this->falseLabel : $value;
	}

}
