<?php

namespace backend\components\grid;

use yii\grid\DataColumn;

/**
 * YesNoColumn отображает колонки Да-Нет
 *
 * ```php
 * 'columns' => [
 * 	[
 *		'class' => 'backend\components\grid\EnumColumn',
 *		'enum' => [
 *			'M' =>'Male',
 *			'F' =>'Female'
 *		],
 *		'attribute' => 'sex'
 * 	]
 * ```
 *
 * @author feugene.org
 */
class EnumColumn extends DataColumn
{

	public $enum = [];

	public function init()
	{
		$this->filter = $this->enum;

		parent::init();
	}

	/**
	 * @inheritdoc
	 */
	public function getDataCellValue($model, $key, $index)
	{
		$value = parent::getDataCellValue($model, $key, $index);

		if ($value !== null) {
			return isset($this->enum[$value]) ? $this->enum[$value] : null;
		}
		return null;
	}

}
