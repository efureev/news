<?php
namespace backend\components\Exceptions;

class NotFoundException extends \yii\base\UserException {

	public function getName()
	{
		return 'Не найдено';
	}

};
