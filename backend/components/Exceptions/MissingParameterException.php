<?php
namespace backend\components\Exceptions;

class MissingParameterException extends \yii\base\UserException {

	public function getName()
	{
		return 'Осутсвует необходимый параметр';
	}

};
