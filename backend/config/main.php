<?php
$params = array_merge(
	require(__DIR__ . '/../../common/config/params.php'),
	require(__DIR__ . '/../../common/config/params-local.php'),
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);

return [
	'id' => 'app-backend',
	'basePath' => dirname(__DIR__),
	'controllerNamespace' => 'backend\controllers',
	'bootstrap' => ['log'],
	'modules' => [
		'backuprestore' => [
			'class' => 'efureev\backuprestore\Module',
			'path' => '@backend/_dumps',
			'ignoreTables' => ['user', 'migration']
		],
		'gridview' => [
			'class' => 'kartik\grid\Module'
		]
	],
	'components' => [
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
				//'dashboard' => 'site/index',
				// [ 'class' => 'yii\rest\UrlRule', 'controller' => 'api/aliases' ],
				'<controller>/<id:\d+>' => '<controller>/view',
			],
		],
		/*  'urlManagerFrontend' => [
			  // here is your frontend URL manager config
		  ],*/
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],

		'authClientCollection' => [
			'class' => 'yii\authclient\Collection',
			'clients' => [
				'feugene' => [
					'class' => 'backend\assets\AuthClient',
					'apiBaseUrl' => 'http://auth.feugene.org/oauth',
					'authUrl' => 'http://auth.feugene.org/oauth/authorize',
					'tokenUrl' => 'http://auth.feugene.org/oauth/token',
					'clientId' => 'newsburg',
					'clientSecret' => 'Iwf13jaSg9G3fE',
				],
			],
		],
		'formatter' => [
			'timeZone' => 'Asia/Yekaterinburg',
			'dateFormat' => 'd.MM.Y',
			'timeFormat' => 'H:mm',
		],
		'user' => [
			'identityClass' => 'common\models\User',
			'class' => 'common\components\web\User',
			'enableAutoLogin' => true,
			'loginUrl' => ['site/auth', 'authclient' => 'feugene'],
		],

	],
	'params' => $params,
];
