<?php

namespace backend\controllers;

use common\models\Category;
use common\models\News;
use Yii;
use common\models\Warehouse;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsersController implements the CRUD actions for Warehouse model.
 */
class UsersController extends BaseAuthController
{

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post']
				],
			],
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'index' => [
				'class' => 'common\actions\users\IndexAction',
			]
		];
	}

}
