<?php
namespace backend\controllers;

use common\models\User;
use Yii;
use yii\authclient\ClientInterface;

use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'actions' => [
							'auth',        // Экшин Yii2 для обработки OAuth2 авторизации
							'no-auth',     // Специальная страница, куда попадает пользователь, отклонивший запрос на сайте авторизации
							'about'
						],
						'allow' => true,
						'roles' => ['?'],
					],
					[
						//'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					]
                ],
            ],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'index' => [ 'get' ],
					'about' => [ 'get' ],
					'auth' => [ 'get' ],
					'no-auth' => [ 'get' ],
					'contact' => [ 'get' ],
					'login' => [ 'get' ],
					'logout' => [ 'post' ]
				],
			]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
			'auth' => [
				'class' => 'yii\authclient\AuthAction',
				'successCallback' => [ $this, 'onOAuthSuccess' ],
				'cancelUrl' => '/no-auth',
			],
        ];
    }

	/**
	 * Обработчик события успешной авторизации через OAuth2
	 *
	 * @param ClientInterface $client
	 * @return bool
	 */
	public function onOAuthSuccess(ClientInterface $client)
	{
		$userInfo = $client->getUserAttributes();

		$user = User::findIdentity($userInfo['id']);
		if ($user === null) {
			$user = new User;
		}
		$user->nickname = $userInfo['login'];
		$user->lastname = $userInfo['lastname'];
		$user->firstname = $userInfo['firstname'];
		$user->email = $userInfo['email'];
		$user->save();
		return app()->user->login($user);
	}

	/**
	 * Специальная страничка, на которую происходит редирект в том случае, если при OAuth2 авторизации на странице
	 * auth.e96.ru пользователь нажал "Отмена" (или сервер авторизации отклонил запрос). На этой страничке просто
	 * показывается текст о том, что для данного сервиса авторизация обязательна и предложение авторизоваться.
	 *
	 * Данный текст показывается только в том случае, если пользователь действительно пришёл с сайта авторизации
	 * и в GET-параметрах соответствующая ошибка. Для этого анализируется Referer заголовок. Если этого заголовка нет
	 * (например, человек напрямую открыл эту страницу в браузере) или если адрес в заголовке не совпадает с тем,
	 * что мы ожидаем, то происходит редирект на главную страницу. Откуда в свою очередь пользователя снова перекидывает
	 * на сайта авторизации.
	 *
	 * @return string|\yii\web\Response
	 */
	public function actionNoAuth()
	{
		// Ставим HTTP-заголовок "Vary", которым говорим, что содержимое этой страницы зависит от HTTP-заголовка "Referer"
		Yii::$app->getResponse()->getHeaders()->set('Vary', 'Referer');

		// Получаем HTTP заголовок "Referer"
		$referer = Yii::$app->request->getHeaders()->get('Referer');

		do {
			// Выход если нет Referer'а
			if (! $referer )
				break;

			// Выход если в Referer невалидный URL
			if (! $pathInfo = parse_url($referer) )
				break;

			// Выход если в пути URL содержится что-либо отличное от "/auth"
			if (empty($pathInfo['path']) || $pathInfo['path'] != '/auth')
				break;

			// Парсим QUERY-строку в массив GET-параметров
			$params = [];
			parse_str($pathInfo['query'], $params);

			// Выход если нет GET-параметра "error" или если он не "access_denied"
			if (empty($params['error']) || $params['error'] != 'access_denied')
				break;

			// Выход если нет GET-параметра "error_message" или если он не содержит "denied the request"
			if (empty($params['error_message']) || stripos($params['error_message'], 'denied the request') === false)
				break;

			// Все проверки пройдены — это тот URL, который мы ожидаем
			return $this->render('no-auth');

		} while (false);

		// Что-то пошло не так — редирект на главную
		return $this->goHome();
	}


    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
		return $this->goHome();
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
