<?php

namespace backend\controllers;

use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;

/**
 * ServicesController implements the CRUD actions for Warehouse model.
 */
class ServicesController extends BaseAuthController
{

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		]);
	}


	public function actions()
	{
		return [
			'index' => [
				'class' => 'common\actions\services\IndexAction',
			],
			'delete' => [
				'class' => 'common\actions\services\DeleteAction',
			],
			'update' => [
				'class' => 'common\actions\services\UpdateAction',
			],
			'view' => [
				'class' => 'common\actions\services\ViewAction',
			],
			'create' => [
				'class' => 'common\actions\services\CreateAction',
			]
		];
	}
}
