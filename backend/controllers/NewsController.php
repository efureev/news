<?php

namespace backend\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;

/**
 * NewsController implements the CRUD actions for Warehouse model.
 */
class NewsController extends BaseAuthController
{

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
					'delete-full' => ['post'],
				],
			],
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'index' => [
				'class' => 'common\actions\news\IndexAction',
			],
			'delete' => [
				'class' => 'common\actions\news\DeleteAction',
			],
			'delete-full' => [
				'class' => 'common\actions\news\DeleteFullAction',
			],
			'update' => [
				'class' => 'common\actions\news\UpdateAction',
			],
			'view' => [
				'class' => 'common\actions\news\ViewAction',
			],
			'create' => [
				'class' => 'common\actions\news\CreateAction',
			]
		];
	}

}
