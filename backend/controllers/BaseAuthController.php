<?php

namespace backend\controllers;

use yii\filters\AccessControl;

abstract class BaseAuthController extends \yii\web\Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			]
		];
	}

	public function renderJson($data)
	{
		app()->response->format = 'json';
		return $data;
	}

}
