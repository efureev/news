<?php
/**
 * @copyright Copyright (c) 2015 Feugene.org
 */

namespace backend\widgets;

use common\components\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;

class TopMenu extends \yii\bootstrap\Widget
{

    public function init() {
        NavBar::begin([
            'brandLabel' => 'News-Burg: Backend',
            'brandUrl' => app()->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);
		$menuItems = [];
        if (!app()->user->isGuest) {

			$menuItems[] = [
				'label' => Html::tag('span','', ['class'=>'glyphicon glyphicon-list']),
				'encode' => false,
				'url' => '/news',
				'options' => ['title' => 'Новости']
			];

			$menuItems[] = [
				'label' => 'Справочники',
				'caret' => true,
				'items' => [
					[
						'label' => 'Сервисы новостей',
						'url' => ['/services']
					],
					[
						'label' => 'Категории',
						'url' => ['/category']
					],
					[
						'label' => 'Соотношение категорий',
						'url' => ['/crosscategory']
					],
					[
						'label' => 'Регионы',
						'url' => ['/region']
					],
					[
						'label' => 'Пользователи',
						'url' => ['/users']
					]
				]
			];

			$menuItems[] = [
				'label' => Html::tag('span','', ['class'=>'glyphicon glyphicon-star']),
				'encode' => false,
				'url' => '@web.frontend',
				'options' => ['title' => 'Frontend']
			];

            $menuItems[] = [
                'label' =>
                    Html::tag('span', app()->user->fullname, ['class'=>'username']) .
                    ' ' .
                    Html::a(Html::tag('span','', ['class'=>'glyphicon glyphicon-th']), '/cabinet') .
                    ' | '.
                    Html::a(Html::tag('span','', ['class'=>'glyphicon glyphicon-log-out']), '/site/logout',['data-method'=>"post"]),
                'tag' => 'p',
                'encode' => false,
                'options' => ['class' => 'navbar-text navbar-right']
            ];
        }

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
        ]);
        NavBar::end();
    }
}