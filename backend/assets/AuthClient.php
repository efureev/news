<?php

namespace backend\assets;

use \yii\authclient\OAuth2;

class AuthClient extends OAuth2
{
	protected function initUserAttributes()
	{
		$data = $this->api('account');

		// Переименование поля "login" в "username"
//		$data['username'] = $data['login'];
//		unset($data['login']);

		return $data;
	}
}
