<?php


/* @var $this yii\web\View */
/* @var $model common\models\Region */

$this->title = $model->name;

$this->params['breadcrumbs'][] = ['label' => 'Регионы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="region-update">

	<?= $this->render('_form', [
			'model' => $model,
		]);
	?>

</div>
