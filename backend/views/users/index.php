<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
?>
<div class="users-index">

	<?= GridView::widget([
		'dataProvider' => $dataProvider,

		'columns' => [
			[
				'format' => 'html',
				'attribute' => 'photo',
				'contentOptions' => ['class' => 'photo'],
				'value' => function ($model) {
					return $model->photo ? '<img src="'.$model->photo .'">' : null;
				},
			],
			'nickname',
			[
				'attribute' => 'ФИО',
				'value' => function ($model) {
					return $model->lastname . ' ' . $model->firstname;
				},
			],
			'email:email',
			[
				'class' => 'backend\components\grid\EnumColumn',
				'enum' => [
					'M' =>'Мужик',
					'F' =>'Баба'
				],
				'attribute' => 'sex'
			],
			[
				'attribute' => 'created_at',
				'value' => function ($model) {
					return app()->formatter->asDatetime($model->created_at, 'php:d M | H:i');
				},
			],

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
