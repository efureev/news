<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
?>
<div class="news-index">
    <p>
        <?= Html::a('Добавить Новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model, $key, $index, $grid) {
			$opt = [];
			if (!$model->visible)
				$opt['class'] = 'deleted';
			return $opt;
		},
        'columns' => [
			[
				'format' => 'html',
				'value'=>function ($model) {
					return '<img src="https://www.google.com/s2/favicons?domain='.$model->service->web.'">';
				},
			],

			[
				'attribute' => 'date',
				'value'=>function ($model) {
					return app()->formatter->asDatetime($model->date,'php:d M | H:i');
				},
			],

			[
				'format' => 'raw',
				'attribute' => 'title',
				'value'=>function ($model) {
					$img = '';

					/** @var common\models\NewsResources $res */
					foreach($model->resources as $res) {
						if ($res->isPhoto)
							$img.= '<a class="img'.($res->isMain ? ' main' : '').'" href="'.$res->link.'"><span class="glyphicon glyphicon-picture"></span></a>';
						else
							$img.= '<a target="_blank" class="video" href="'.$res->link.'"><span class="glyphicon glyphicon-facetime-video"></span></a>';
					}

					return '<p>'.$model->title.'</p>' .
					($model->categoryName ? '<small>'.$model->categoryName .'</small>': false).
						'<span> | </span>'.$img;
				},
			],

			[
				'format' => 'raw',
				'value'=>function ($model) {
					return  '<a target="_blank" href="'.$model->link.'">'.Html::tag('span','', ['class'=>'glyphicon glyphicon-link']).'</a>';
				},
			],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
