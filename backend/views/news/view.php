<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;

?>
<div class="news-view">
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Удалить', ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => 'Вы точно хотите удалить новость?',
				'method' => 'post',
			],
		]) ?>
		<?= Html::a('Уничтожить', ['delete-full', 'id' => $model->id], [
			'class' => 'btn btn-danger right',
			'data' => [
				'confirm' => 'Вы точно хотите уничтожить новость?',
				'method' => 'post',
			],
		]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
			'service.name',
			'category.name',
			'title',
			'text:html',

			[
				'attribute' => 'resources',
				'format' => 'raw',
				'value'=> implode('',array_map(function($res){
					if ($res->isPhoto) {
						$r = '<div class="photo'.($res->isMain ? ' main' : '').'"><img src="'.$res->link.'"></div>';
					} else {
						$r = '<div class="video"><iframe allowfullscreen="" frameborder="0" height="500" src="'.$res->link.'" width="100%"></iframe></div>';
					}
					return $r;
				},$model->resources))
			],

		],

    ]) ?>

</div>
