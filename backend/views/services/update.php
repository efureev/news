<?php


/* @var $this yii\web\View */
/* @var $model common\models\NewsService */

$this->title = $model->name;

$this->params['breadcrumbs'][] = ['label' => 'Сервис новостей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="newsService-update">

	<?
	if (!\common\models\Region::find()->count()) {
		echo '<p>Нет ни одного Региона</p><p>'.\yii\helpers\Html::a('добавить Регион',['/region/create']).'</p>';
	} else {
		echo $this->render('_form', [
			'model' => $model,
		]);
	}
	?>

</div>
