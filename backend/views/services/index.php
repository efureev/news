<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сервисы новостей';
?>
<div class="newsService-index">
    <p>
        <?= Html::a('Добавить сервис', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

			'region.name',
			'name',
			'charset',
//			'tag',
			[
				'class' => 'backend\components\grid\YesNoColumn',
				'attribute' => 'enable'
			],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
