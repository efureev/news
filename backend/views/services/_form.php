<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\NewsService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newsService-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'tag')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'web')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'regionId')->dropDownList(
		ArrayHelper::map(\common\models\Region::find()->orderBy('name')->all(),'id', 'name')) ?>

	<?= $form->field($model, 'enable')->checkbox() ?>

	<?= $form->field($model, 'emailHarvest')->checkbox() ?>

	<?= $form->field($model, 'rss')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'fullload')->textInput() ?>

	<?= $form->field($model, 'charset')->dropDownList([
			'UTF-8'=> 'UTF-8',
			'WINDOWS-1251' => 'WINDOWS-1251',
		]
	) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
