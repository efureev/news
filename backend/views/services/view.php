<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\NewsService */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Сервисы новостей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;

?>
<div class="newsService-view">
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно хотите удалить данный сервис?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
			'name',
			'tag'
        ],
    ]) ?>

</div>
