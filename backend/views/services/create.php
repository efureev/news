<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NewsService */

$this->title = 'Создать сервис';
$this->params['breadcrumbs'][] = ['label' => 'Сервисы новостей', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создание';
?>
<div class="newsService-create">

    <h1><?= Html::encode($this->title) ?></h1>

	<?
	if (!\common\models\Region::find()->count()) {
		echo '<p>Нет ни одного Региона</p><p>'.Html::a('добавить Регион',['/region/create']).'</p>';
	} else {
		echo $this->render('_form', [
			'model' => $model,
		]);
	}?>


</div>
