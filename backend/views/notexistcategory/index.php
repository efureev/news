<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\NewsServiceCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Не присвоенные категории';
?>
<div class="notexistcategory-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'name',
				'label' => 'Название категории у сервиса',
			],
			[
				'attribute' => 'serviceId',
				'value' => 'service.name',
			],
            [
				'class' => 'yii\grid\ActionColumn',
				'template' => '{add} {delete}',
				'buttons' =>[
					'add' => function ($url, $model, $key) {
						return Html::a(Html::tag('span', '', [
							'class' => "glyphicon glyphicon-plus",
							'title' => 'Добавить к NB в категории'
						]), [
							'notexistcategory/addcategory', 'id' => $model->id
						],[
							'data'=>[
								'method'=>'post',
								'pjax'=>'0'
							]
						]);
					},
				]
			],
        ],
    ]); ?>

</div>