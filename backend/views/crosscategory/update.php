<?php


/* @var $this yii\web\View */
/* @var $model common\models\NewsServiceCategory */

$this->title = $model->name;

$this->params['breadcrumbs'][] = ['label' => 'Категории сервисов новостей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="crosscategory-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
