<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\NewsServiceCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории сервисов новостей';
?>
<div class="crosscategory-index">
    <p>
        <?= Html::a('Добавить Категорию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'categoryId',
				'value' => 'category.name',
				'label' => 'Категория NB',
				'filter' => Html::activeDropdownList($searchModel, 'categoryId', ArrayHelper::map(\common\models\Category::find()->orderBy('name')->all(),'id', 'name'),['prompt' => 'Выберите категорию...']),
			],
			[
				'attribute' => 'name',
				'label' => 'Название категории у сервиса',
			],
			[
				'attribute' => 'serviceId',
				'value' => 'service.name',
				'filter' => Html::activeDropdownList($searchModel, 'serviceId', ArrayHelper::map(\common\models\NewsService::find()->orderBy('name')->all(),'id', 'name'),['prompt' => 'Выберите сервис...']),
			],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>