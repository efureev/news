<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\NewsServiceCategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории сервисов новостей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;

?>
<div class="crosscategory-view">
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно хотите удалить категорию?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
			'name',
			'tag'
        ],
    ]) ?>

</div>
