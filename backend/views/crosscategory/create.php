<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NewsServiceCategory */

$this->title = 'Создать категорию сервиса Новостей';
$this->params['breadcrumbs'][] = ['label' => 'Категории сервисов новостей', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создание';
?>
<div class="crosscategory-create">

    <h1><?= Html::encode($this->title) ?></h1>

	<?
	if (!\common\models\NewsService::find()->count()) {
		echo '<p>Нет ни одного сервиса новостей</p><p>'.Html::a('добавить сервис новостей',['/services/create']).'</p>';
	} else if (!\common\models\Category::find()->count()) {
		echo '<p>Нет ни одной категории</p><p>'.Html::a('добавить категорию',['/category/create']).'</p>';
	} else {
			echo $this->render('_form', [
        		'model' => $model,
    		]);
		}?>

</div>
