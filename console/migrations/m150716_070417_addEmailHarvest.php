<?php

use yii\db\Schema;
use yii\db\Migration;

class m150716_070417_addEmailHarvest extends Migration
{
    public function up()
    {
        $this->addColumn('news_services','emailHarvest', Schema::TYPE_SMALLINT. ' NOT NULL DEFAULT 0 COMMENT "Собирает новости в почте"');
    }

    public function down()
    {
        $this->dropColumn('news_services','emailHarvest');
    }

}
