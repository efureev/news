<?php

use yii\db\Schema;
use yii\db\Migration;

class m150614_210623_addregion extends Migration
{
    public function up()
    {
		$this->addColumn('news_services','regionId', Schema::TYPE_SMALLINT. ' NOT NULL COMMENT "id Региона"');
    }

    public function down()
    {
        $this->dropColumn('news_services','regionId');
    }

}
