<?php

use yii\db\Schema;
use yii\db\Migration;

class m150109_013608_add_socialUser extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user_social', [
            'id' => Schema::TYPE_BIGPK,
            'userId' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "id основного пользователя"',
            'firstname' => Schema::TYPE_STRING . '(50) NOT NULL COMMENT "Имя"',
            'lastname' => Schema::TYPE_STRING . '(50) NOT NULL COMMENT "Фамилия"',
            'nickname' => Schema::TYPE_STRING . '(50) NOT NULL COMMENT "nickname"', // nickname, screen_name, etc..
            'short_pathname' => Schema::TYPE_STRING . '(50) NOT NULL COMMENT "короткое имя, указываемое в пути домена сети"',
            'dob' => Schema::TYPE_DATE . ' NOT NULL COMMENT "День рождения"',
            'uid' => Schema::TYPE_STRING . '(32) NOT NULL COMMENT "id в сети"',
            'network' => Schema::TYPE_STRING . '(20) NOT NULL COMMENT "id Сети"',
            'email' => Schema::TYPE_STRING . ' NULL COMMENT "email регистрации в сети"',
            'photo' => Schema::TYPE_STRING . ' NULL COMMENT "фото в сети"',
            'phone' => Schema::TYPE_STRING . '(11) NULL COMMENT "Телефон в сети"',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            "UNIQUE KEY `networkId` (`uid`,`network`)"
        ], $tableOptions);

        $this->addColumn('user','sex','enum("F","M") default null COMMENT "Пол пользователя" after `username`');
        $this->addColumn('user','timezone', Schema::TYPE_SMALLINT.' default null COMMENT "Таймзона юзера" after `status`');
        $this->renameColumn('user','username', 'nickname');
        $this->addColumn('user','lastname', Schema::TYPE_STRING . '(50) COMMENT "Фамилия" after `nickname`');
        $this->addColumn('user','firstname', Schema::TYPE_STRING . '(50) COMMENT "Имя" after `lastname`');
        $this->addColumn('user','photo', Schema::TYPE_STRING . ' COMMENT "Фото" after `email`');
    }

    public function down()
    {
        $this->dropTable('user_social');
        $this->dropColumn('user', 'sex');
        $this->dropColumn('user', 'timezone');
        $this->dropColumn('user', 'lastname');
        $this->dropColumn('user', 'firstname');
        $this->dropColumn('user', 'photo');
        $this->renameColumn('user','nickname', 'username');
    }
}
