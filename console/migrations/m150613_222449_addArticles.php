<?php

use yii\db\Schema;
use yii\db\Migration;

class m150613_222449_addArticles extends Migration
{
    public function up()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('news', [
			'id' => Schema::TYPE_BIGPK,
			'title' => Schema::TYPE_STRING . '(250) NOT NULL COMMENT "Название"',
			'description' => Schema::TYPE_TEXT . ' NULL COMMENT "Описание краткое"',
			'visible' => Schema::TYPE_BOOLEAN. ' NOT NULL DEFAULT 1 COMMENT "Показывать"',
			'link' => Schema::TYPE_STRING . '(250) NULL COMMENT "Ссылка на оригинальную статью"',
			'serviceId' => Schema::TYPE_SMALLINT. ' NOT NULL COMMENT "id сервиса новостей"',
			'categoryId' => Schema::TYPE_SMALLINT. ' NOT NULL COMMENT "id Категории"',
			'regionId' => Schema::TYPE_SMALLINT. ' NOT NULL COMMENT "id Региона"',
			'guid' => Schema::TYPE_STRING . '(40) NULL COMMENT "Уникальный идентификатор на сервисе, если есть"',
			'important' => Schema::TYPE_BOOLEAN. ' NOT NULL DEFAULT 0 COMMENT "Важная новость"',
			'views' => Schema::TYPE_SMALLINT. ' NOT NULL DEFAULT 0 COMMENT "Количество просмотров"',
			'text' => Schema::TYPE_TEXT. ' NULL COMMENT "Текст новости"',
			'date' => Schema::TYPE_DATETIME. ' NOT NULL COMMENT "Дата публикации"',
			'hash' => Schema::TYPE_STRING . '(32) NOT NULL COMMENT "Хеш по линку"',
			'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
			"KEY `visible` (`visible`)",
			"KEY `serviceId` (`serviceId`)",
			"KEY `regionId` (`regionId`)"
		], $tableOptions." COMMENT='Новости'");

		$this->createTable('news_resources', [
			'id' => Schema::TYPE_BIGPK,
			'title' => Schema::TYPE_STRING . '(250) NULL COMMENT "Название"',
			'visible' => Schema::TYPE_BOOLEAN. ' NOT NULL DEFAULT 1 COMMENT "Показывать"',
			'link' => Schema::TYPE_STRING . '(250) NULL COMMENT "Ссылка на картинку"',
			'newsId' => Schema::TYPE_BIGINT. ' NOT NULL COMMENT "id новости"',
			'type' => 'enum("photo","video") NOT NULL COMMENT "Тип ресурса"',
			'main' => Schema::TYPE_BOOLEAN. ' NOT NULL DEFAULT 0 COMMENT "Показывать на главной странице"',
			'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
			"KEY `visible` (`visible`)",
			"KEY `newsId` (`newsId`)",
			"KEY `type` (`type`)"
		], $tableOptions." COMMENT='Новостные ресурсы'");


		$this->createTable('regions', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . "(100) NOT NULL UNIQUE",
		], $tableOptions." COMMENT='Регионы'");

    }

    public function down()
    {
        $this->dropTable('news');
        $this->dropTable('news_resources');
        $this->dropTable('regions');
    }

}
