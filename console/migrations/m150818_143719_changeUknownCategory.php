<?php

use yii\db\Schema;
use yii\db\Migration;

class m150818_143719_changeUknownCategory extends Migration
{
    public function up()
    {
		$this->update('news',['categoryId'=>100], 'categoryId = 19');
    }

    public function down()
    {
		$this->update('news',['categoryId'=>19], 'categoryId = 100');
    }
}
