<?php

use yii\db\Schema;
use yii\db\Migration;

class m150630_062059_RBAC_addModeratorRules extends Migration
{
    public function up()
    {
		$auth = app()->authManager;

		// add "createNews" permission
		$createNews = $auth->createPermission('newsCreate');
		$createNews->description = 'Create a news';
		$auth->add($createNews);

		// add "updateNews" permission
		$updateNews = $auth->createPermission('newsUpdate');
		$updateNews->description = 'Update news';
		$auth->add($updateNews);

		// add "newsDelete" permission
		$newsDelete = $auth->createPermission('newsDelete');
		$newsDelete->description = 'Delete news';
		$auth->add($newsDelete);

		// add "newsChangeImportant" permission
		$newsChangeImportant = $auth->createPermission('newsChangeImportant');
		$newsChangeImportant->description = 'Change news important';
		$auth->add($newsChangeImportant);

		// add "newsChangeVisible" permission
		$newsChangeVisible = $auth->createPermission('newsChangeVisible');
		$newsChangeVisible->description = 'Change news visible';
		$auth->add($newsChangeVisible);

		// add "author" role and give this role the "createNews" permission
		$author = $auth->createRole('author');
		$author->description = 'Автор';
		$auth->add($author);

		$auth->addChild($author, $createNews);
		$auth->addChild($author, $newsChangeVisible);

		$admin = $auth->getRole('admin');

		$moderator = $auth->createRole('moderator');
		$moderator->description = 'Модератор';
		$auth->add($moderator);
		$auth->addChild($moderator, $updateNews);
		$auth->addChild($moderator, $newsDelete);
		$auth->addChild($moderator, $newsChangeImportant);
		$auth->addChild($moderator, $author);
		$auth->addChild($admin, $moderator);

		// Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
		// usually implemented in your User model.
//		$auth->assign($author, 2);
//		$auth->assign($admin, 1);
    }

    public function down()
    {
		$auth = app()->authManager;
		$admin = $auth->getRole('admin');
		$author = $auth->getRole('author');
		$moderator = $auth->getRole('moderator');

		$updateNews = $auth->getPermission('newsUpdate');
		$createNews = $auth->getPermission('newsCreate');
		$newsDelete = $auth->getPermission('newsDelete');
		$newsChangeImportant = $auth->getPermission('newsChangeImportant');
		$newsChangeVisible = $auth->getPermission('newsChangeVisible');

		$auth->removeChild($admin, $moderator);
		$auth->removeChild($moderator, $author);
		$auth->removeChild($moderator, $updateNews);
		$auth->removeChild($moderator, $newsDelete);
		$auth->removeChild($moderator, $newsChangeImportant);
		$auth->remove($updateNews);
		$auth->remove($newsDelete);
		$auth->remove($newsChangeImportant);

		$auth->remove($moderator);

		$auth->removeChild($author, $createNews);
		$auth->removeChild($author, $newsChangeVisible);
		$auth->remove($newsChangeVisible);
		$auth->remove($createNews);

		$auth->remove($author);




    }
}
