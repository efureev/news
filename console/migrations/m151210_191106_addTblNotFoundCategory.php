<?php

use yii\db\Schema;
use yii\db\Migration;

class m151210_191106_addTblNotFoundCategory extends Migration
{
    public function up()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('notExistCategory', [
			'id' => Schema::TYPE_BIGPK,
			'name' => Schema::TYPE_STRING . '(250) NOT NULL COMMENT "Название"',
			'serviceId' => Schema::TYPE_SMALLINT. ' NOT NULL COMMENT "id сервиса новостей"',
			"KEY `serviceId` (`serviceId`)",
			"UNIQUE KEY `name_service` (`serviceId`,`name`)",
		], $tableOptions." COMMENT='Не найденные категории сервисов'");

    }

    public function down()
    {
		$this->dropTable('notExistCategory');
    }


}
