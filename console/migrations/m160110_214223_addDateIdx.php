<?php

use yii\db\Schema;
use yii\db\Migration;

class m160110_214223_addDateIdx extends Migration
{
    public function up()
    {
		$this->createIndex('date','news','date');
    }

    public function down()
    {
		$this->dropIndex('date','news');
    }

}
