<?php

use yii\db\Schema;
use yii\db\Migration;

class m150810_191644_userConfig extends Migration
{
    public function up()
    {
		$this->addColumn('user','newsConf', Schema::TYPE_TEXT. ' NULL COMMENT "Настройки показа новостей для пользователя"');
    }

    public function down()
    {
		$this->dropColumn('user','newsConf');
    }
}
