<?php

use yii\db\Schema;
use yii\db\Migration;

class m151210_211912_addIndex extends Migration
{
    public function up()
    {
		$this->createIndex('hash','news','hash',true);
    }

    public function down()
    {
		$this->dropIndex('hash','news');
    }


}
