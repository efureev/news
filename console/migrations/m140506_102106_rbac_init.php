<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

use yii\base\InvalidConfigException;
use yii\db\Schema;
use yii\rbac\DbManager;

use common\rbac\rules\AuthorRule;

/**
 * Initializes RBAC tables
 *
 * @author Alexander Kochetov <creocoder@gmail.com>
 * @since 2.0
 */
class m140506_102106_rbac_init extends \yii\db\Migration
{
    /**
     * @throws yii\base\InvalidConfigException
     * @return DbManager
     */
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }
        return $authManager;
    }

    public function up()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($authManager->ruleTable, [
            'name' => Schema::TYPE_STRING . '(64) NOT NULL',
            'data' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
            'PRIMARY KEY (name)',
        ], $tableOptions);

        $this->createTable($authManager->itemTable, [
            'name' => Schema::TYPE_STRING . '(64) NOT NULL',
            'type' => Schema::TYPE_INTEGER . ' NOT NULL',
            'description' => Schema::TYPE_TEXT,
            'rule_name' => Schema::TYPE_STRING . '(64)',
            'data' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
            'PRIMARY KEY (name)',
            'FOREIGN KEY (rule_name) REFERENCES ' . $authManager->ruleTable . ' (name) ON DELETE SET NULL ON UPDATE CASCADE',
        ], $tableOptions);
        $this->createIndex('idx-auth_item-type', $authManager->itemTable, 'type');

        $this->createTable($authManager->itemChildTable, [
            'parent' => Schema::TYPE_STRING . '(64) NOT NULL',
            'child' => Schema::TYPE_STRING . '(64) NOT NULL',
            'PRIMARY KEY (parent, child)',
            'FOREIGN KEY (parent) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY (child) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->createTable($authManager->assignmentTable, [
            'item_name' => Schema::TYPE_STRING . '(64) NOT NULL',
            'user_id' => Schema::TYPE_STRING . '(64) NOT NULL',
            'created_at' => Schema::TYPE_INTEGER,
            'PRIMARY KEY (item_name, user_id)',
            'FOREIGN KEY (item_name) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $auth = app()->authManager;
        $auth->removeAll();

        // Rules
        $authorRule = new AuthorRule();
        $auth->add($authorRule);

        // Permissions
        $accessBackend = $auth->createPermission('accessBackend');
        $accessBackend->description = 'Can access backend';
        $auth->add($accessBackend);

        $administrateRbac = $auth->createPermission('administrateRbac');
        $administrateRbac->description = 'Can administrate all "RBAC" module';
        $auth->add($administrateRbac);

        $BViewRoles = $auth->createPermission('BViewRoles');
        $BViewRoles->description = 'Can view roles list';
        $auth->add($BViewRoles);

        $BCreateRoles = $auth->createPermission('BCreateRoles');
        $BCreateRoles->description = 'Can create roles';
        $auth->add($BCreateRoles);

        $BUpdateRoles = $auth->createPermission('BUpdateRoles');
        $BUpdateRoles->description = 'Can update roles';
        $auth->add($BUpdateRoles);

        $BDeleteRoles = $auth->createPermission('BDeleteRoles');
        $BDeleteRoles->description = 'Can delete roles';
        $auth->add($BDeleteRoles);

        $BViewPermissions = $auth->createPermission('BViewPermissions');
        $BViewPermissions->description = 'Can view permissions list';
        $auth->add($BViewPermissions);

        $BCreatePermissions = $auth->createPermission('BCreatePermissions');
        $BCreatePermissions->description = 'Can create permissions';
        $auth->add($BCreatePermissions);

        $BUpdatePermissions = $auth->createPermission('BUpdatePermissions');
        $BUpdatePermissions->description = 'Can update permissions';
        $auth->add($BUpdatePermissions);

        $BDeletePermissions = $auth->createPermission('BDeletePermissions');
        $BDeletePermissions->description = 'Can delete permissions';
        $auth->add($BDeletePermissions);

        $BViewRules = $auth->createPermission('BViewRules');
        $BViewRules->description = 'Can view rules list';
        $auth->add($BViewRules);

        $BCreateRules = $auth->createPermission('BCreateRules');
        $BCreateRules->description = 'Can create rules';
        $auth->add($BCreateRules);

        $BUpdateRules = $auth->createPermission('BUpdateRules');
        $BUpdateRules->description = 'Can update rules';
        $auth->add($BUpdateRules);

        $BDeleteRules = $auth->createPermission('BDeleteRules');
        $BDeleteRules->description = 'Can delete rules';
        $auth->add($BDeleteRules);

        // Assignments
        $auth->addChild($administrateRbac, $BViewRoles);
        $auth->addChild($administrateRbac, $BCreateRoles);
        $auth->addChild($administrateRbac, $BUpdateRoles);
        $auth->addChild($administrateRbac, $BDeleteRoles);
        $auth->addChild($administrateRbac, $BViewPermissions);
        $auth->addChild($administrateRbac, $BCreatePermissions);
        $auth->addChild($administrateRbac, $BUpdatePermissions);
        $auth->addChild($administrateRbac, $BDeletePermissions);
        $auth->addChild($administrateRbac, $BViewRules);
        $auth->addChild($administrateRbac, $BCreateRules);
        $auth->addChild($administrateRbac, $BUpdateRules);
        $auth->addChild($administrateRbac, $BDeleteRules);

        // Roles
        $user = $auth->createRole('user');
        $user->description = 'User';
        $auth->add($user);

        $admin = $auth->createRole('admin');
        $admin->description = 'Admin';
        $auth->add($admin);
        $auth->addChild($admin, $user);

        $superadmin = $auth->createRole('root');
        $superadmin->description = 'Super pooper Man';
        $auth->add($superadmin);
        $auth->addChild($superadmin, $admin);
        $auth->addChild($superadmin, $accessBackend);
        $auth->addChild($superadmin, $administrateRbac);

        $auth->assign($superadmin, 1);
    }

    public function down()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        $this->dropTable($authManager->assignmentTable);
        $this->dropTable($authManager->itemChildTable);
        $this->dropTable($authManager->itemTable);
        $this->dropTable($authManager->ruleTable);
    }
}
