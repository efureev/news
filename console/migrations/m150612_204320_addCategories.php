<?php

use yii\db\Schema;
use yii\db\Migration;

class m150612_204320_addCategories extends Migration
{
    public function up()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('categories', [
			'id' => Schema::TYPE_BIGPK,
			'name' => Schema::TYPE_STRING . '(100) NOT NULL COMMENT "Название категории" UNIQUE',
			'tag' => Schema::TYPE_STRING . '(50) NOT NULL COMMENT "Тэг категории" UNIQUE',
			'visibleInList' => Schema::TYPE_SMALLINT. ' NOT NULL DEFAULT 1 COMMENT "Показывать в списке категорий"',
			'enable' => Schema::TYPE_SMALLINT. ' NOT NULL DEFAULT 1 COMMENT "Включенная категория"',
			'sort' => Schema::TYPE_INTEGER . ' NOT NULL Default "0" COMMENT "Сортировка в листе"',
			'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
			"KEY `visibleInList` (`visibleInList`)",
			"KEY `enable` (`enable`)"
		], $tableOptions." COMMENT='Категории новостей'");

		$this->createTable('news_services', [
			'id' => Schema::TYPE_BIGPK,
			'name' => Schema::TYPE_STRING . '(100) NOT NULL COMMENT "Название Внешнего Сервиса" UNIQUE',
			'tag' => Schema::TYPE_STRING . '(50) NOT NULL COMMENT "Тэг Внешнего сервиса" UNIQUE',
			'enable' => Schema::TYPE_SMALLINT. ' NOT NULL DEFAULT 0 COMMENT "Включенный сервис"',
			'web' => Schema::TYPE_STRING . '(250) NOT NULL COMMENT "официальный сайт сарвиса"',
			'params' => Schema::TYPE_TEXT.' NULL COMMENT "Параметры сервиса"',
			'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
			"KEY `enable` (`enable`)"
		], $tableOptions." COMMENT='Соответствие категорий внишних сервисов с нашими'");

		$this->createTable('news_services_categories', [
			'id' => Schema::TYPE_BIGPK,
			'name' => Schema::TYPE_STRING . '(100) NOT NULL COMMENT "Название категории"',
			'tag' => Schema::TYPE_STRING . '(50) NOT NULL COMMENT "Тэг категории"',
			'serviceId' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "id сервиса"',
			'categoryId' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "id категории"',
			'enable' => Schema::TYPE_SMALLINT. ' NOT NULL DEFAULT 1 COMMENT "Включенная категория"',
			'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
			"KEY `serviceId` (`serviceId`)",
			"KEY `categoryId` (`categoryId`)",
			"UNIQUE KEY `name_service` (`name`, `serviceId`)"
		], $tableOptions." COMMENT='Соответствие категорий внишних сервисов с нашими'");

		$this->insert('categories',[ 'name'=> 'город', 'tag'=> 'city' ]);
		$this->insert('categories',[ 'name'=> 'общество', 'tag'=> 'society' ]);
		$this->insert('categories',[ 'name'=> 'происшествия', 'tag'=> 'incidents' ]);
		$this->insert('categories',[ 'name'=> 'авто', 'tag'=> 'auto' ]);
		$this->insert('categories',[ 'name'=> 'экономика', 'tag'=> 'economics' ]);
		$this->insert('categories',[ 'name'=> 'политика', 'tag'=> 'politics' ]);
		$this->insert('categories',[ 'name'=> 'бизнес', 'tag'=> 'business' ]);
		$this->insert('categories',[ 'name'=> 'развлечения', 'tag'=> 'entertainment' ]);
		$this->insert('categories',[ 'name'=> 'отдых', 'tag'=> 'relax' ]);
		$this->insert('categories',[ 'name'=> 'спорт', 'tag'=> 'sport' ]);
		$this->insert('categories',[ 'name'=> 'события', 'tag'=> 'events' ]);
		$this->insert('categories',[ 'name'=> 'здоровье', 'tag'=> 'health' ]);
		$this->insert('categories',[ 'name'=> 'образование', 'tag'=> 'education' ]);
		$this->insert('categories',[ 'name'=> 'интернет', 'tag'=> 'inet' ]);
		$this->insert('categories',[ 'name'=> 'технологии', 'tag'=> 'tech' ]);
		$this->insert('categories',[ 'name'=> 'криминал', 'tag'=> 'crime' ]);

		$this->insert('news_services',[ 'name'=> '66.ru', 'tag'=> '66ru', 'web'=>'66.ru' ]);
    }

    public function down()
    {
        $this->dropTable('categories');
        $this->dropTable('news_services');
        $this->dropTable('news_services_categories');
    }

}
