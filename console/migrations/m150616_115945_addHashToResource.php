<?php

use yii\db\Schema;
use yii\db\Migration;

class m150616_115945_addHashToResource extends Migration
{
    public function up()
    {
		$this->addColumn('news_resources','hash', Schema::TYPE_STRING . '(32) NOT NULL COMMENT "Хеш по линку"');

		$all = \common\models\NewsResources::find()->all();
		foreach ($all as $on) {
			$on->hash = \common\models\NewsResources::getHashFromLink($on->link);
			$on->update();
		}
		$this->dropColumn('news','date');
		$this->addColumn('news','date', Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Дата"');
    }

    public function down()
    {
		$this->dropColumn('news_resources','hash');
    }

}
