<?php

namespace console\controllers;

use common\components\NewsService\NewsService;
use common\models\News;
use yii\console\Controller;

/**
 * Service console controller.
 */
class NewsServiceController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public $defaultAction = 'run';


	public function actionRun($serviceId = null)
	{
		if ($serviceId !== null) {
			/** @var  \common\models\NewsService $service */
			$service = \common\models\NewsService::findOne($serviceId);
			NewsService::run($service);
		} else {
			/** @var  \common\models\NewsService[] $services */
			$services = \common\models\NewsService::find()->enable()->all();
			foreach ($services as $service)
				NewsService::run($service);
		}
	}

	public function actionLoad($serviceId = null, $limit = 30)
	{
		$emptyNews = News::find()->emptyBody()->limit($limit)->orderBy('date ASC');

		if ($serviceId !== null) {
			$emptyNews = $emptyNews->services($serviceId);
		}
		$emptyNews = $emptyNews->all();

		include_once \Yii::getAlias('@vendor') . '/electrolinux/phpquery/phpQuery/phpQuery.php';

		/** @var  \common\models\NewsService $service */
		foreach ($emptyNews as $news)
			NewsService::getFullPage($news);
	}

	public function actionClear()
	{
		NewsService::clear();
	}
}