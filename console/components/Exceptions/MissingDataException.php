<?php
namespace console\components\Exceptions;

class MissingDataException extends \yii\base\UserException {

	public function getName()
	{
		return 'Осутсвует необходимые данные';
	}

};
