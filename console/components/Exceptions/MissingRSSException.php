<?php
namespace console\components\Exceptions;

class MissingRSSException extends MissingDataException {

	public function getName()
	{
		return 'Осутсвует RSS лента';
	}

};
