<?php


namespace console\components;

use yii\helpers\Console;

trait Help{

	/**
	 * Вывод ошибок в консоли
	 * @param $string
	 *
	 * @return int
	 */
	public static function errorMsg($string)
	{
		return fwrite(\STDERR, Console::ansiFormat($string.PHP_EOL, [Console::FG_RED]));
	}

	public static function successMsg($string)
	{
		return fwrite(\STDERR, Console::ansiFormat($string.PHP_EOL, [Console::FG_GREEN]));
	}

	public static function warnMsg($string)
	{
		return fwrite(\STDERR, Console::ansiFormat($string.PHP_EOL, [Console::FG_YELLOW]));
	}

	public static function msg($string)
	{
		return fwrite(\STDERR, $string.PHP_EOL);
	}
}

