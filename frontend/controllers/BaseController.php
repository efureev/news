<?php

namespace frontend\controllers;

use yii\web\Controller;

abstract class BaseController extends Controller {


	/**
	 * @inheritdoc
	 */
	public function beforeAction($action)
	{
		if (app()->user->can('moderator')) {
			app()->params['topMenuItem'][] =
				[
					'label' => 'Справочники',
					'caret' => true,
					'items' => [
						[
							'label' => 'Новости',
							'url' => ['/adm/news']
						],
						[
							'label' => 'Сервисы новостей',
							'url' => ['/adm/services']
						],
						[
							'label' => 'Категории сервисов',
							'url' => ['/adm/crosscategory']
						],
						[
							'label' => 'Неизвестные Категории',
							'url' => ['/adm/notexistcategory']
						],
						[
							'label' => 'Пользователи',
							'url' => ['/adm/users']
						]
					]
				];
		}
		return parent::beforeAction($action);
	}


}
