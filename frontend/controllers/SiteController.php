<?php
namespace frontend\controllers;

use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\ErrorException;

use yii\base\InvalidParamException;
use yii\validators\UrlValidator;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\UserSocial;
use common\models\User;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
		return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }



	/**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'authSuccessCallback'],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function authSuccessCallback(\yii\authclient\OAuth2 $client) {
		$userInfo = $client->getUserAttributes();

		/** @var UserSocial $UserSocial */
		$UserSocial = UserSocial::findOne([
			'uid' => $userInfo['uid'],
			'network' => $userInfo['network']]);

		if ($UserSocial === null) {
			if (app()->user->isGuest) {
				$email = isset($userInfo['email']) ? $userInfo['email'] : false;

				if ($email) {
					if (!$user = User::findByEmail($email)) {
						/** @var UserSocial $UserSocialFind */
						if ($UserSocialFind = UserSocial::findByEmail($email)) {
							$user = $UserSocialFind->user;
						}
					}
				}
			} else {
				$user = app()->user->identity;
			}

			if (!$user instanceof User) {
				$user = new User();
				if (!$user->save(false))
					throw new ErrorException('Ошибка сохранения пользователя ' . print_r($user->errors, 1));
			}

			$userId = $user->id;

			$UserSocial = new UserSocial;
			$UserSocial->attributes = $userInfo;
			$UserSocial->userId = $userId;
			if (!$UserSocial->save())
				throw new ErrorException('Ошибка сохранения соцсети пользователя ' . print_r($UserSocial->errors, 1));
		} else {
			$user = $UserSocial->user;
		}


        if (empty($user->nickname)) {
            $user->nickname = !empty($UserSocial->nickname) ? $UserSocial->nickname : $UserSocial->fullName;
        }

        if (empty($user->lastname) && !empty($UserSocial->lastname)) {
            $user->lastname = $UserSocial->lastname;
        }

        if (empty($user->firstname) && !empty($UserSocial->firstname)) {
            $user->firstname = $UserSocial->firstname;
        }

        if (empty($user->photo) && !empty($UserSocial->photo)) {
            $user->photo = $UserSocial->photo;
        }
        if (empty($user->sex) && !empty($userInfo['sex'])) {
            $user->sex = $userInfo['sex'];
        }

        if (empty($user->timezone) && !empty($userInfo['timezone'])) {
            $user->timezone = $userInfo['timezone'];
        }

        if (empty($user->email) && !empty($UserSocial->email)) {
            $user->email = $UserSocial->email;
        }

        $user->save(false);
		$mp = \Mixpanel::getInstance(app()->params['MIXPANEL_TOKEN']);
		$mp->identify($user->id);
		$mp->people->increment($user->id, "login count", 1);
		$mp->track("Logged In");

		return app()->user->login($user);
    }

    public function actionLogin()
    {
        if (!app()->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(app()->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

	public function actionLogout()
	{
		app()->user->logout();

		return $this->goHome();
	}

	public function actionGo($url)
	{
		if (!(new UrlValidator())->validate($url))
			throw new BadRequestHttpException('Не правильный URL');

		app()->response->redirect($url);
	}

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(app()->request->post()) && $model->validate()) {
            if ($model->sendEmail(app()->params['adminEmail'])) {
                app()->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                app()->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(app()->request->post())) {
            if ($user = $model->signup()) {
                if (app()->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(app()->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                app()->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                app()->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(app()->request->post()) && $model->validate() && $model->resetPassword()) {
            app()->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
