<?php
namespace frontend\controllers;

use frontend\models\News;
use frontend\widgets\ModeratorMenu;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;


/**
 * News controller
 */
class AjaxController extends Controller
{


	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['news-hide', 'signup'],
				'rules' => [
					[
						'actions' => ['signup'],
						'allow' => true,
						'roles' => ['?'],
					],
					[
						'actions' => ['news-hide'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'*' => ['post'],
					'list' => ['get'],
				],
			],
		];
	}

	public function renderJson($data)
	{
		app()->response->format = 'json';
		return $data;
	}

	public function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			if (!app()->request->isAjax)
				throw new BadRequestHttpException();
			return true;
		} else {
			return false;
		}
	}

	public function actionHide()
	{
		if (!app()->user->can('newsChangeVisible'))
			throw new ForbiddenHttpException('У вас нет прав на совершение этого действия');

		return $this->renderJson([
			'title' => $this->postModel()->visible ? 'показать' : 'скрыть',
			'status' => $this->postModel()->toggleVisible()
		]);
	}

	public function actionList($id)
	{
		if (!$id)
			throw new NotFoundHttpException('Новость не найдена');

		$items = News::listToArray(
			News::find()
				->where('date > :date', [
					':date' => $this->findModel($id)->date
				])
				->{app()->user->can('moderator') ? 'allNews' : 'enable'}()
				->orderBy('date DESC')
				->all()
		);


		return $this->renderJson([
			'items' => $items
		]);
	}

	public function actionImportant()
	{
		if (!app()->user->can('newsChangeVisible'))
			throw new ForbiddenHttpException('У вас нет прав на совершение этого действия');

		return $this->renderJson([
			'title' => $this->postModel()->important ? 'важное' : 'не важное',
			'status' => $this->postModel()->toggleImportant()
		]);
	}


	/**
	 * Показывает новость
	 *
	 * @return string
	 * @throws ForbiddenHttpException
	 * @throws NotFoundHttpException
	 */
	public function actionsShow()
	{
		if (!app()->user->can('newsChangeVisible'))
			throw new ForbiddenHttpException('У вас нет прав на совершение этого действия');

		return $this->renderJson([
			'status' => $this->postModel()->setVisible()
		]);
	}

	public function actionNewsImportant()
	{
		if (!app()->user->can('newsChangeImportant'))
			throw new ForbiddenHttpException('У вас нет прав на совершение этого действия');

		return $this->renderJson([
			'status' => $this->postModel()->setImportant()
		]);
	}

	public function actionNewsNormal()
	{
		if (!app()->user->can('newsChangeImportant'))
			throw new ForbiddenHttpException('У вас нет прав на совершение этого действия');

		return $this->renderJson([
			'status' => $this->postModel()->setNotImportant()
		]);
	}

	/**
	 * @param $id
	 *
	 * @return News|null|static
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id)
	{
		if (($model = News::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	/**
	 * @return News
	 * @throws NotFoundHttpException
	 */
	protected function postModel()
	{
		$post = app()->request->post();
		$id = isset($post['id']) ? (int)$post['id'] : null;

		if (!$id)
			throw new NotFoundHttpException('Новость не найдена');

		return $this->findModel($id);
	}


}
