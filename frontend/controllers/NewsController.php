<?php
namespace frontend\controllers;

use common\models\Category;
use common\models\NewsService;
use frontend\models\News;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;


/**
 * News controller
 */
class NewsController extends BaseController
{

	public function actionIndex() {
		$model = News::find()->loaded();
		if ($categoriesEnable = Category::getEnableForUser())
			$model->category($categoriesEnable);

		if ($servicesEnable = NewsService::getEnableForUser())
			$model->services($servicesEnable);

		$model->toDateTime(new \DateTime());

		if (!app()->user->can('moderator'))
			$model->enable();

		$dataProvider = new ActiveDataProvider([
			'pagination' => [
				'pageSize' => 20,
			],
			'query' => $model->orderBy('date DESC')
		]);
		return $this->render('index', [
			'dataProvider' => $dataProvider
		]);
	}

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionView($id) {
		$model = $this->findModelNews($id);

		if ($model->text === null) {
			app()->response->redirect($model->link);
		}

		return $this->render('view', [
			'news' => $model
		]);
	}

	public function actionCategory($category) {
		$model = $this->categoryByTag($category);

		$dataProvider = new ActiveDataProvider([
			'pagination' => [
				'pageSize' => 20,
			],
			'query' => News::find()->enable()->category($model->id)->orderBy('date DESC')
		]);
		return $this->render('index', [
			'dataProvider' => $dataProvider
		]);
	}

	/**
	 * @param $id
	 *
	 * @return News
	 * @throws NotFoundHttpException
	 */
	protected function findModelNews($id)
	{
		if ((($model = News::findOne($id)) !== null)) {
			return $model;
		} else {
			throw new NotFoundHttpException('Новость не найдена');
		}
	}

	/**
	 * @param $tag
	 *
	 * @return Category
	 * @throws NotFoundHttpException
	 */
	protected function categoryByTag($tag)
	{
		if (($model = Category::find()->where('tag = :tag',[':tag'=>mb_strtolower($tag)])->one()) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('Категория не найдена');
		}
	}

}
