<?php
namespace frontend\models;

use yii\helpers\Html;


/**
 * News model
 *
 * @property string $categoryName
 * @property string $resourceBlock
 * @property string $serviceIconNewsLink
 * @property string $categoryNameBlock
 * @property string $editLink
 */
class News extends \common\models\News
{

    public function getCategoryNameBlock()
    {
        return $this->categoryName ? '| <a class="category" itemprop="articleSection" content="' . $this->categoryName . '" href="/' . $this->category->tag . '">' . $this->categoryName . '</a>' : false;
    }

    public function getServiceIconNewsLink()
    {
        return Html::a($this->serviceIcon, '/go/' . $this->link, ['class' => 'service', 'target' => '_blank']);
    }

    public function getEditLink()
    {
        return \Yii::getAlias(app()->params['backend'] ? '@web.backend' : '@web.frontend.adm') . '/news/' . $this->id;
    }

    public function getResourceBlock()
    {
        $result = [
            'photo' => false,
            'video' => false
        ];

        foreach ($this->resources as $res) {
            if ($res->isPhoto)
                $result['photo'] = true;
            else
                $result['video'] = true;
        }

        return $result['photo'] && $result['video']
            ? '| <div class="res">' . ($result['photo'] ? '<span class="photo">фото</span>' : '') . ($result['video'] ? ', <span class="video">видео</span>' : '') . '</div>'
            : '';
    }

}
