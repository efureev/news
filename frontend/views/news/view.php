<?php
/* @var $this \yii\web\View
 * @var $news \frontend\models\News
 */
$this->title = 'News-Burg.ru: ' . $news->title;

$this->registerMetaTag([
    'name' => 'og:description',
    'content' => $news->description
]);
?>
<article class="news-view" data-key="<?= $news->id ?>" itemscope itemtype="http://schema.org/NewsArticle">
    <h1 class="news-title" itemprop="name"><?= $news->title ?></h1>
    <div class="news-info">
        <meta itemprop="datePublished" content="<?= app()->formatter->asDate($news->date, 'Y-MM-dd') ?>">
        <span class="date"><?= $news->date ?></span>
        <?= $news->categoryNameBlock ?>
        | <?= $news->serviceIconNewsLink ?>
        | <?= \frontend\widgets\Share::widget([
            'news' => $news
        ]); ?>
        <?= $admMenu = \frontend\widgets\ModeratorMenu::widget(['news' => $news]) ?>
    </div>
    <div
        class="news-preview"><?= (!empty($news->preview) ? \yii\helpers\Html::img($news->preview->link, ['itemprop' => 'image']) : '') ?></div>
    <div class="news-body" itemprop="description"><?= $news->text ?></div>
    <?= \frontend\widgets\Gallery::widget([
        'resources' => $news->gallery,
        'visibleMain' => false
    ]);
    ?>
    <?= $admMenu ?>
</article>
