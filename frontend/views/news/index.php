<?php
/* @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 *
 */
$this->title = 'News-Burg.ru';
$this->registerMetaTag([
	'name' => 'og:description',
	'content' => "NEWS-BURG.RU: Новости города и области (18+)"
]);
?>

<?= \yii\widgets\ListView::widget([
	'summary' => false,
	'emptyText' => 'Нет новостей',
	'dataProvider' => $dataProvider,
	'itemView' => '_news',
	'itemOptions' => [
		'tag'=>'article',
	],
	'options' => [
		'class' => 'list-news',
		'tag' => 'section'
	]
]);