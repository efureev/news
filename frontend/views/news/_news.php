<?php
/**
 * @var \frontend\models\News $model
 */
use yii\helpers\Html;
use frontend\widgets\ModeratorMenu;

?>
<div class="time"><?= $model->date ?></div>
<h2>
	<a href="/<?= $model->id ?>"><?= Html::encode($model->title) ?></a>
</h2>
<div class="desc">
	<?= $model->serviceIconNewsLink ?>
	<span><?= $model->description ?></span>
</div>
<div class="foot">
	<span class="date"><?= $model->date ?></span>
	<?= $model->categoryNameBlock ?>
	<?= $model->views > 0 ? '| <span class="eye">просмотрено: ' . $model->views . '</span>' : '' ?>
	<?= $model->resourceBlock ?>

	<?
	if ($model->important)
		echo '| <span class="label label-danger news-btn news-important">важное</span>';

	echo ModeratorMenu::widget(['news' => $model,'prefix'=>' | adm: ']);
	?>
</div>