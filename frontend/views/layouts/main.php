<?php
use frontend\assets\AppAsset;
use frontend\widgets\Footer;
use frontend\widgets\TopMenu;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;


/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= app()->language ?>">
<head>
    <meta charset="<?= app()->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <meta http-equiv="X-UA-Compatible" content="chrome=IE8">
    <title><?= Html::encode($this->title) ?></title>

    <meta name="twitter:site" content="news_burg_ru"/>
    <meta name="twitter:creator" content="@Fureev"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?= app()->request->getAbsoluteUrl() ?>"/>
    <meta property="og:title" content="<?= Html::encode($this->title) ?>"/>
    <meta property="og:site_name" content="NEWS-BURG.RU"/>
    <meta property="og:image" content=""/>
    <meta name="keywords"
          content="екатеринбург, новости екатеринбурга, происшествия, екатеринбург сегодня, новости свердловской области, сайт новостей, последние новости, криминальные новости, news-burg.ru, главные новости, новости онлайн, регионы">
    <meta property="fb:app_id" content="489825551032773"/>
    <meta property="vk:title" content="NEWS-BURG.RU: Новости города и области"/>

    <link rel="shortcut icon" href="http://news-burg.ru/assets/images/favicon16.png">

    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/favicon72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/favicon144.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/assets/images/favicon192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/favicon96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon16.png">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">

    <?= TopMenu::widget(); ?>

    <div class="container">

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= $content ?>
    </div>
</div>


<?= Footer::widget() ?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
