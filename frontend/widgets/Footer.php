<?php
/**
 * @copyright Copyright © 2015 Feugene.org
 */

namespace frontend\widgets;

class Footer extends \yii\bootstrap\Widget
{

    public function run() {
		return $this->render('footer/main');
    }
}