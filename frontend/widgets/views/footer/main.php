<footer>
	<? if (!YII_ENV_DEV) {
		echo \frontend\widgets\Counters::widget(['counters' => [
			'yandex' => [
				'showInformer' => false,
				'enable' => true
			],
			'LiveInternet' => [
				'showInformer' => false,
				'enable' => true
			],
			'GA' => [
				'enable' => true
			],
			'mixpanel' => [
				'enable' => true
			],
		]

		]);
	}?>
	<div class="context">
		<div><a href="http://news-burg.ru/">News-Burg.ru</a> - сегодняшние новости Екатеринбурга и Урала</div>
		<div>Присоединяйтесь к нам в социальных сетях: <a href="/go/http://vk.com/news_burg_ru" target="_blank">Вконтакте</a>, <a href="/go/https://www.facebook.com/NewsBurgRu" target="_blank">Facebook</a></div>
<!--		<a href="http://news-burg.ru/adv/">Реклама на сайте</a>-->
	</div>
</footer>