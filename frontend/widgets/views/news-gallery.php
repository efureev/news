<div class="news-gallery">
    <?
    $photos = [];
    $video = [];
    /** @var \common\models\NewsResources[] $resources */
    foreach ($resources as $resource) {
        if ($resource->isPhoto) {
            $photos[] =
                [
                    'url' => $resource->link,
                    'src' => $resource->link,
                    'imageOptions' => ['itemprop' => 'image']
                ];
        }
        if ($resource->isVideo) {
            $video[] = $resource->link;
        }
    }
    ?>

    <?= dosamigos\gallery\Gallery::widget(['items' => $photos]); ?>
    <? foreach ($video as $v) {
        echo '<iframe frameborder="0" height="300" src="' . $v . '" width="535"></iframe>';
    } ?>

</div>

