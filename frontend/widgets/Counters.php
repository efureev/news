<?php
/**
 * @copyright Copyright © 2015 Feugene.org
 */

namespace frontend\widgets;

class Counters extends \yii\bootstrap\Widget
{

	public $counters = [];

	public $content;
	public $showInformers = false;

	public function init() {
		parent::init();

		foreach ($this->counters as $counter => $data) {
			if (!$data['enable'])
				continue;
			if (!isset($data['showInformer']))
				$data['showInformer'] = false;
			if ($data['showInformer'])
				$this->showInformers = true;

			$this->content .= $this->render('footer/counters/'.$counter, ['showInformer' => $data['showInformer']]);
		}
	}


	public function run() {
		return !empty($this->content) ? '<div class="counters'.($this->showInformers?' informers':'').'">'.$this->content.'</div>' : '';
	}
}