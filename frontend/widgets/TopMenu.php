<?php
/**
 * @copyright Copyright (c) 2014 Feugene.org
 */

namespace frontend\widgets;

use common\components\bootstrap\Nav;
use efureev\fontawesome\FA;
use yii\bootstrap\NavBar;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class TopMenu extends \yii\bootstrap\Widget
{

    public function init()
    {
        NavBar::begin([
            'brandLabel' => app()->name,
            'brandUrl'   => app()->homeUrl,
            'options'    => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);

        $menuItems[] = [
            'label'   => FA::stack(['data-role' => 'stacked-icon'])
                ->on(FA::Icon('circle-thin', ['style' => 'font-size: 2.5em;margin-top: -3px;margin-left: -1px;']))
                ->icon(FA::Icon('paper-plane-o', ['style' => 'margin-left: -1px;'])),
            'encode'  => false,
            'url'     => 'https://telegram.me/newsburg',
            'linkOptions' => ['target'=>'_blank'],
            'options' => [
                'title' => 'Канал в telegram: news-burg',
            ],
            'caret'   => false,
        ];

        if (app()->user->isGuest) {
            $menuItems[] = [
                'label'  => Html::tag('span', '', ['class' => 'glyphicon glyphicon-off']),
                'encode' => false,
                'caret'  => false,
                'items'  => [
                    auth::widget()
                ]
            ];
        } else {
            $role = app()->authManager->getRolesByUser(app()->user->id);
            $role = reset($role);
            if ($role instanceof \yii\rbac\Role) {
                $roleName = $role->name;
            } else
                $roleName = 'none';

            $menuItems[] = [
                'label'   => ($role === 'user' ? '' : Html::tag('span', mb_strtoupper($roleName{0}), ['class' => 'label label-success']) . ' ') .
                    Html::tag('span', app()->user->fullname, ['class' => 'username']) .
                    ' ' .
                    Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-th']), '/cabinet') .
                    ' | ' .
                    Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-log-out']), '/site/logout', ['data-method' => "post"]),
                'tag'     => 'p',
                'encode'  => false,
                'options' => ['class' => 'navbar-text navbar-right']
            ];
        }

        if (!empty(app()->params['topMenuItem']) && is_array(app()->params['topMenuItem'])) {
            $menuItems = ArrayHelper::merge($menuItems, app()->params['topMenuItem']);
        }

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items'   => $menuItems,
        ]);
        NavBar::end();
    }
}