<?php
/**
 * @copyright Copyright © 2015 Feugene.org
 */

namespace frontend\widgets;

class Share extends \yii\base\Widget
{
	/** @var \common\models\News|null  */
	public $news = null;

    public function run() {
		return $this->render('share',[
			'url' => app()->request->absoluteUrl,
			'title' => $this->news->title,
			'image' => empty($this->news->preview) ? '': $this->news->preview->link,
			'desc' => $this->news->description,
		]);
    }
}