<?php
/**
 * @copyright Copyright © 2015 Feugene.org
 *
 *            Виджет Категорий
 */

namespace frontend\widgets;

use common\models\Category;

class CategoriesMenu extends \yii\bootstrap\Widget
{
	public function run() {
		$categories = Category::find()->enable()->inList()->all();
		if (empty($categories))
			return false;

		return $this->render('categories-menu',['categories'=>$categories]);
    }
}