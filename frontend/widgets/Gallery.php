<?php
/**
 * @copyright Copyright (c) 2014 Feugene.org
 */

namespace frontend\widgets;

class Gallery extends \yii\bootstrap\Widget
{

	public $resources = [];
	public $visibleMain = true;

	public function run() {
		if (!count($this->resources))
			return false;
		return $this->render('news-gallery',[
			'resources' => $this->resources,
			'visibleMain' => $this->visibleMain
		]);
	}
}