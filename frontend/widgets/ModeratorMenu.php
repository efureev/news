<?php
/*
 * @copyright Copyright (c) 2015 Feugene.org
 */

namespace frontend\widgets;

use frontend\models\News;
use \yii\base\Widget;
use yii\helpers\Html;

class ModeratorMenu extends Widget
{

	/**
	 * @var null|News
	 */
	public $news = null;
	public $prefix = null;


	public function run()
	{
		$block = [];

		if (app()->user->can('newsChangeVisible')) {
			$block[] = Html::tag('span', !$this->news->visible ? 'показать' :'скрыть', ['class' => 'label label-'.(!$this->news->visible ? 'danger':'info').' news-btn news-hide']);
		}

		if (app()->user->can('newsChangeImportant')) {
			$block[] = Html::tag('span', $this->news->important ? 'не важное' :'важное',['class' => 'label label-'.($this->news->important ? 'danger':'info').' news-btn news-important']);
		}

		if (app()->user->can('newsUpdate')) {
			$block[] = Html::a('редактировать',$this->news->editLink,['class' => 'label label-info']);
		}

		if (count($block)) {
			return ($this->prefix ? $this->prefix : ''). implode(' ', $block);
		}
		return '';
	}
}