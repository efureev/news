<?php
/**
 * @copyright Copyright (c) 2014 Feugene.org
 */

namespace frontend\widgets;

use yii\authclient\widgets\AuthChoice;

class auth extends \yii\bootstrap\Widget
{

    public function init() {
        parent::init();

        $authAuthChoice = AuthChoice::begin([
            'baseAuthUrl' => ['site/auth'],
            'options' => ['class'=>'auth-wrapper']
        ]);

        foreach ($authAuthChoice->getClients() as $client) {
            $client->setTitle('');
            echo $authAuthChoice->clientLink($client);
        }

        AuthChoice::end();
    }

}