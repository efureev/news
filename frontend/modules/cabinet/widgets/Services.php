<?php
/**
 * @copyright Copyright (c) 2015 Feugene.org
 */

namespace frontend\modules\cabinet\widgets;

use common\models\NewsService;
use yii\base\Widget;

class Services extends Widget
{

	public function run()
	{
		$services = NewsService::find()->enable()->all();
		$servicesEnable = NewsService::getEnableForUser();

		$cities = [];
		/** @var NewsService[] $services */
		foreach ($services as $service) {
			if (!isset($cities[$service->regionId]))
				$cities[$service->regionId]['region'] = [
					'id' => $service->regionId,
					'name' => $service->region->name,
				];

			$cities[$service->regionId]['services'][$service->id] = $service;

		}
		return $this->render('services/index', ['cities' => $cities, 'enabled' => $servicesEnable]);
	}

}