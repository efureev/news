<div class="cabinet-news-services">
	<h4>Выберите Сервис, который вы хотите видеть в ленте: </h4>
	<?
	/**
	 * @var array $cities
	 * @var array $enabled
	 */

	foreach ($cities as $city) { ?>
		<div class="city"><?= $city['region']['name'] ?></div>
		<?= \yii\helpers\Html::checkboxList('services', $enabled, \yii\helpers\ArrayHelper::map($city['services'], 'id', 'name'), ['class' => 'services']);
	}?>

</div>
