<div class="cabinet-news-categories">
	<h4>Выберите категории, которые хотите видеть в ленте: </h4>
	<?
	/** @var common\models\Category[] $categories */
	echo \yii\helpers\Html::checkboxList('categories', $enabled, \yii\helpers\ArrayHelper::map($categories, 'id', 'name'), ['class' => 'categories']); ?>
</div>
