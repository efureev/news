<?php
/**
 * @copyright Copyright (c) 2015 Feugene.org
 */

namespace frontend\modules\cabinet\widgets;

use common\models\Category;
use yii\base\Widget;

class Categories extends Widget
{

	public function run()
	{
		$query = Category::find();

		if (!app()->user->can('moderator'))
			$query = $query->enable();

		$categories = $query->all();
		$categories[] = Category::getUnknownCategory();
		$categoriesEnable = Category::getEnableForUser();

		return $this->render('categories/index', ['categories' => $categories, 'enabled' => $categoriesEnable]);
	}

}