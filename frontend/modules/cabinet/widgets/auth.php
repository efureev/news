<?php
/**
 * @copyright Copyright (c) 2014 Feugene.org
 */

namespace frontend\modules\cabinet\widgets;

use yii\authclient\widgets\AuthChoice;

class auth extends \yii\base\Widget {

    public function init() {
        parent::init();

        $authAuthChoice = AuthChoice::begin([
            'baseAuthUrl' => ['default/auth'],
            'options' => ['class'=>'auth-wrapper']
        ]);

        $clients = app()->authClientCollection->getClients();

        array_map ( function($item) use (&$clients) {
                unset($clients[$item->network]);
                return $item->network;
            },
            app()->user->identity->social
        );

        if (empty($clients))
            return;
        echo '<h3>не связанные аккаунты: </h3>';
        foreach ($clients as $client) {
            $client->setTitle('');
            $authAuthChoice->clientLink($client);
        }
        $authAuthChoice->autoRender = false;
        AuthChoice::end();
    }

}