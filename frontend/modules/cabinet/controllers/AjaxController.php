<?php
namespace frontend\modules\cabinet\controllers;

use common\models\Category;
use common\models\NewsService;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;


/**
 * News controller
 */
class AjaxController extends Controller
{


	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['news-hide', 'signup'],
				'rules' => [
					// разрешено неавторизованным пользователям
					[
						'allow' => false,
						'roles' => ['?'],
					],
					//разрешено авторизованным пользователям
					[
//                        'actions' => ['logout','about'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'save-category' => ['post'],
				],
			],
		];
	}

	public function renderJson($data)
	{
		app()->response->format = 'json';
		return $data;
	}

	public function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			if (!app()->request->isAjax)
				throw new BadRequestHttpException();
			return true;
		} else {
			return false;
		}
	}


	public function actionSaveCategory()
	{
		if (app()->user->isGuest)
			throw new ForbiddenHttpException();

		$post = app()->request->post();
		/** @var \common\models\User $user */
		$user = app()->user->identity;
		$newsConf = $user->newsConf;

		if (isset($newsConf['categories']))
			$userCat = $newsConf['categories'];
		else
			$userCat = [];

		if (Category::isUnknown($post['id'])) {
			$category = Category::getUnknownCategory();
		} elseif (!$category = Category::findOne($post['id']))
			throw new NotFoundHttpException('The requested page does not exist.');

		$enable = $post['value'] === 'true' ? true : false;

		if ($enable) {
			$userCat[$category->id] = 1;
		} else {
			unset($userCat[$category->id]);
		}

		$newsConf['categories'] = $userCat;

		if (empty($newsConf['categories']))
			unset($newsConf['categories']);

		$user->setAttribute('newsConf', $newsConf);

		return $this->renderJson([
			'status' => (int)$user->save()
		]);
	}


	public function actionSaveService()
	{
		if (app()->user->isGuest)
			throw new ForbiddenHttpException();

		$post = app()->request->post();
		/** @var \common\models\User $user */
		$user = app()->user->identity;
		$newsConf = $user->newsConf;

		if (isset($newsConf['services']))
			$userService = $newsConf['services'];
		else
			$userService = [];

		if (!$service = NewsService::findOne($post['id']))
			throw new NotFoundHttpException('The requested page does not exist.');

		$enable = $post['value'] === 'true' ? true : false;

		if ($enable) {
			$userService[$service->id] = 1;
		} else {
			unset($userService[$service->id]);
		}

		$newsConf['services'] = $userService;

		$allServices = NewsService::find()->select('id')->column();

		if (empty(array_diff($allServices,array_keys($userService))))
			$newsConf['services'] = [];

		if (empty($newsConf['services']))
			unset($newsConf['services']);

		$user->setAttribute('newsConf', $newsConf);

		return $this->renderJson([
			'status' => (int)$user->save()
		]);
	}


}
