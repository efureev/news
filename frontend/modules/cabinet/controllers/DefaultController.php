<?php
namespace frontend\modules\cabinet\controllers;

use frontend\modules\cabinet\widgets\auth,
    common\models\UserSocial;
/**
 * Default controller
 */
class DefaultController extends \yii\web\Controller {

    public function actions() {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'authSuccessCallback']
            ]
        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    // разрешено неавторизованным пользователям
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    //разрешено авторизованным пользователям
                    [
//                        'actions' => ['logout','about'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

	public function actionIndex() {
		$authUnlinked = auth::widget();
		return $this->render('info', [
			'authLinked' => app()->user->identity->social,
			'authUnlinked' => $authUnlinked
		]);
	}

    public function authSuccessCallback($client) {
        $this->action->successUrl = '/cabinet';
        $userInfo = $client->getUserAttributes();
        $UserSocial = UserSocial::findOne([
            'uid' => $userInfo['uid'],
            'network' => $userInfo['network']]);

        if (app()->user->isGuest)
            return;

        if (empty($UserSocial)) {
            $UserSocial = new UserSocial;
            $UserSocial->attributes = $userInfo;
        }

        $UserSocial->userId = app()->user->id;
        $UserSocial->save();

        if (empty($UserSocial->user->nickname)) {
            $UserSocial->user->nickname = !empty($UserSocial->nickname) ? $UserSocial->nickname : $UserSocial->fullName;
        }

        if (empty($UserSocial->user->lastname) && !empty($UserSocial->lastname)) {
            $UserSocial->user->lastname = $UserSocial->lastname;
        }

        if (empty($UserSocial->user->firstname) && !empty($UserSocial->firstname)) {
            $UserSocial->user->firstname = $UserSocial->firstname;
        }

        if (empty($UserSocial->user->photo) && !empty($UserSocial->photo)) {
            $UserSocial->user->photo = $UserSocial->photo;
        }
        if (empty($UserSocial->user->sex) && !empty($userInfo['sex'])) {
            $UserSocial->user->sex = $userInfo['sex'];
        }

        if (empty($UserSocial->user->timezone) && !empty($userInfo['timezone'])) {
            $UserSocial->user->timezone = $userInfo['timezone'];
        }

        if (empty($UserSocial->user->email) && !empty($UserSocial->email)) {
            $UserSocial->user->email = $UserSocial->email;
        }

        $UserSocial->user->save();
        return true;
    }
    
}
