<?php
/**
 * @link <..>
 * @copyright Copyright (c) 2015 FEUGENE.ORG
 */

namespace frontend\modules\cabinet;

use Yii;

class CabinetModule extends \yii\base\Module {

	public $controllerNamespace = 'frontend\modules\cabinet\controllers';

	public $defaultRoute = 'default/info';

}
