<?php

namespace frontend\modules\adm\assets;

use yii\web\AssetBundle;

class AdmAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'assets/css/adm.css',
	];
	public $js = [];
	public $depends = [];

}
