<?php

namespace frontend\modules\adm;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\adm\controllers';

	public $defaultRoute = 'news/index';

}
