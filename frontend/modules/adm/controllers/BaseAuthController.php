<?php

namespace frontend\modules\adm\controllers;

use frontend\modules\adm\assets\AdmAsset;
use yii\filters\AccessControl;
use frontend\controllers\BaseController;

abstract class BaseAuthController extends BaseController {

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'authFilter' => [
				'class' => 'common\components\AuthFilter',
			],
		];
	}

	public function renderJson($data)
	{
		app()->response->format = 'json';
		return $data;
	}

	/**
	 * @inheritdoc
	 */
	public function beforeAction($action)
	{
		AdmAsset::register($this->getView());
		return parent::beforeAction($action);
	}


}
