<?php
namespace frontend\modules\adm\controllers;

use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Site controller
 */
class SiteController extends BaseAuthController
{
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'index' => ['get'],
					'login' => ['get'],
					'logout' => ['post']
				],
			],
		]);
	}

	public function actionIndex()
	{
		return $this->render('@backend/views/site/index');
	}

	public function actionLogin()
	{
		return $this->goHome();
	}

	public function actionLogout()
	{
		app()->user->logout();
		return $this->goHome();
	}
}
