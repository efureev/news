<?php

namespace frontend\modules\adm\controllers;

use common\models\Category;
use common\models\News;
use Yii;
use common\models\Warehouse;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NewsController implements the CRUD actions for Warehouse model.
 */
class NewsController extends BaseAuthController
{

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
					'delete-full' => ['post'],
				],
			],
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'index' => [
				'class' => 'common\actions\news\IndexAction',
			],
			'delete' => [
				'class' => 'common\actions\news\DeleteAction',
			],
			'delete-full' => [
				'class' => 'common\actions\news\DeleteFullAction',
			],
			'update' => [
				'class' => 'common\actions\news\UpdateAction',
			],
			'view' => [
				'class' => 'common\actions\news\ViewAction',
			],
			'create' => [
				'class' => 'common\actions\news\CreateAction',
			]
		];
	}

}
