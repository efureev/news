<?php

namespace frontend\modules\adm\controllers;

use common\models\NewsServiceCategory;
use common\models\NotExistCategory;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NotexistcategoryController
 */
class NotexistcategoryController extends BaseAuthController
{

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
					'addcategory' => ['post'],
				],
			],
		]);
	}

	public function actions()
	{
		return [
			'index' => [
				'class' => 'common\actions\notexistcategory\IndexAction',
			],
			'addcategory'=> [
				'class' => 'common\actions\notexistcategory\AddcategoryAction',
			],
			'delete' => [
				'class' => 'common\actions\notexistcategory\DeleteAction',
			],
		];
	}


    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsServiceCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NotExistCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
