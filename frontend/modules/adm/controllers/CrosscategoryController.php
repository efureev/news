<?php

namespace frontend\modules\adm\controllers;

use common\models\NewsServiceCategory;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CrosscategoryController implements the CRUD actions for Warehouse model.
 */
class CrosscategoryController extends BaseAuthController
{

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		]);
	}

	public function actions()
	{
		return [
			'index' => [
				'class' => 'common\actions\crosscategory\IndexAction',
			],
			'create' => [
				'class' => 'common\actions\crosscategory\CreateAction',
			],
			'delete' => [
				'class' => 'common\actions\crosscategory\DeleteAction',
			],
			'update' => [
				'class' => 'common\actions\crosscategory\UpdateAction',
			],
			'view' => [
				'class' => 'common\actions\crosscategory\ViewAction',
			]
		];
	}


    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsServiceCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsServiceCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
