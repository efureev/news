<?php
namespace app\modules\api\models;

use common\models\User;

class UserRest extends User
{

    public function fields() {
        $fields = parent::fields();

        unset($fields['auth_key'], $fields['password_hash'], $fields['password_reset_token'], $fields['updated_at'], $fields['created_at']);
        $fields['role'] = function ($fields) {
            return $fields['role'] == 50 ? 'admin' : 'user';
        };
        $fields['status'] = function ($fields) {
            return $fields['status'] == 10 ? 'active' : 'deleted';
        };
        return $fields;
    }

}
