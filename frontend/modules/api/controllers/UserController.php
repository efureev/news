<?php
namespace app\modules\api\controllers;

use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
//use yii\filters\auth\QueryParamAuth;
use yii\web\ForbiddenHttpException;
use app\modules\api\filters\restReformatter;

/**
 * User controller
 */
class UserController extends ActiveController {

    public $modelClass = 'app\modules\api\models\UserRest';

    public function init()
    {
        parent::init();
        $this->reConfigureUser();
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['create']);
        $actions['index']['prepareDataProvider'] = [$this, 'selectUser'];

        return $actions;
    }

    public function selectUser() {
//        app()->response->format = 'html';
//        dump(app()->request->headers);
//        die;
        $modelClass = $this->modelClass;
        return new ActiveDataProvider([
            'query' => $modelClass::find(),
        ]);
    }

    private function reConfigureUser()
    {
        $user = \Yii::$app->user;
        $user->enableSession = false;
        $user->enableAutoLogin = false;
        $user->loginUrl = null;
    }

    public function behaviors() {
        $behaviors = parent::behaviors();

        $behaviors['restReformatter'] = [
            'class' => restReformatter::className()
        ];

        /*$behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
        ];*/
        return $behaviors;
    }

    public function checkAccess($action, $model = null, $params = []) {

        /*$request = \Yii::$app->getRequest();
        $accept = $request->getHeaders()->get('accept');

        //dump(app()->request->headers);
//die;
        if (\Yii::$app->user->id == 2)
            throw new ForbiddenHttpException('Доступ закрыт');*/


      //  \Yii::$app->response->format = Response::FORMAT_HTML;


        //$this->formats = ['application/json' => 'json'];
        //unset($this->formats[1]);
       // dump($this->formats);die;
//        $user = \Yii::$app->user;

//        $user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);

//
//        $user->enableAutoLogin = false;
      //  dump($user->id);
      //  die;
    }

    
}
