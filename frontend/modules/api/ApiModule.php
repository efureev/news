<?php
/**
 * @link <..>
 * @copyright Copyright (c) 2014 FEUGENE.ORG
 */

namespace app\modules\api;

use Yii,
    yii\base\Application;

class ApiModule extends \yii\base\Module {

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'yii\api\controllers';


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::configure($this, require(__DIR__ . '/config.php'));
    }


}
