<?php

namespace app\modules\api\filters;

use Yii;
use yii\base\ActionFilter;
use yii\web\NotAcceptableHttpException;

class restReformatter extends ActionFilter
{

    public $acceptMimeType = 'application/json';


    public function beforeAction($action)
    {
        if (Yii::$app->response->acceptMimeType !== $this->acceptMimeType)
            throw new NotAcceptableHttpException('The requested response format is not supported: ' . Yii::$app->response->format );
        return true;
    }
}
