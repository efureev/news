<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                  => 'app-frontend',
    'basePath'            => dirname(__DIR__),
    'name'                => 'News-Burg',
    'modules'             => [
        'api'     => [
            'class' => 'app\modules\api\ApiModule'
        ],
        'cabinet' => [
            'class' => 'frontend\modules\cabinet\CabinetModule'
        ],
        'adm'     => [
            'class' => 'frontend\modules\adm\Module'
        ],
    ],
    'bootstrap'           => [
        'log'
    ],
    'controllerNamespace' => 'frontend\controllers',
    'components'          => [
        'user'         => [
            'class'           => 'common\components\web\User',
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager'   => [
            'enablePrettyUrl' => true,
            //            'enableStrictParsing' => true,
            'showScriptName'  => false,
            'rules'           => [
                'GET go/<url:[-.\w:/]+>'           => 'site/go',
                ''                                 => 'news/index',
                'GET /<module:cabinet>'            => '<module>/default/index',
                'GET /<category:[a-zA-Z]+>'        => 'news/category',
                'GET /<id:\d+>'                    => 'news/view',
                'GET <m:adm>/news/<id:\d+>'        => 'adm/news/view',
                'GET <m:adm>/news/<action:[\w-]+>' => 'adm/news/<action>',
                '<controller:\w+>'                 => '<controller>/index',
                ['class' => 'yii\rest\UrlRule', 'controller' => 'api/user'],
                ['class' => 'yii\rest\UrlRule', 'controller' => ['apost' => 'api/post']],
            ],
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'levels'  => ['error', 'warning'],
                    'logVars' => [],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'cache' => [
            'class'     => 'yii\redis\Cache',
            'redis'     => [
                'hostname' => 'localhost',
                'port'     => 6379,
                'database' => 0,
            ],
            'keyPrefix' => 'nb',
        ],


        'formatter' => [
            'timeZone'       => 'Asia/Yekaterinburg',
            'datetimeFormat' => 'dd MMM Y, H:mm',
            'dateFormat'     => 'd MMM Y',
            'timeFormat'     => 'H:mm',
        ],
    ],
    'params'              => $params,
];
