<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets/css/styles.css',
    ];

    public $depends = [
        'efureev\fontawesome\AssetBundle',
    ];

    public function init() {
        parent::init();

        $view = \Yii::$app->getView();
        if (YII_ENV_DEV) {
            $view->registerJsFile('@web.frontend/assets/js/extra.min.js', ['position' => $view::POS_END]);
            $view->registerJsFile('@web.frontend/assets/js/libs.min.js', ['position' => $view::POS_END]);
            $view->registerJsFile('@web.frontend/assets/js/app.min.js', ['position' => $view::POS_END]);
        } else {
            $view->registerJsFile('@web.frontend/assets/js/js.min.js', ['position' => $view::POS_END]);
        }
    }
}
