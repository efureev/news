$(function () {

	var appProto = function () {

	};

	var news = function () {
			/**
			 * Скрывает новость
			 * @param id
			 */
			var
				/**
				 * Получает новость из списка
				 *
				 * @param id
				 * @returns {*|jQuery|HTMLElement}
				 */
				getNews = function (id) {
					return $('.list-news article[data-key=' + id + ']');
				},

				/**
				 * Возвращает последнюю новость в списке новостей
				 *
				 * @returns {*|jQuery|HTMLElement}
				 */
				getLastNewsInList = function () {
					return $('.list-news article:first');
				},

				getLastNewsId = function () {
					return getLastNewsInList().data('key');
				},

				/**
				 * Возвращает Номер страницы новостей
				 *
				 * @returns {Number}
				 */
				getPageId = function () {
					var query = app.helper.parseCurrentURL().searchObject;

					if (µ.utils.isEmpty(query.page) || query.page == 1)
						return 1;

					return +query.page;
				},

				/**
				 * Переключатель Скрытости новости
				 * @param id
				 * @param el
				 */
				toggleHide = function (id, el) {
					post('/ajax/hide', id, function (data) {
						if (data.status == 1) {
							el.text(data.title);
							if (data.title == 'показать') {
								el.removeClass('label-info').addClass('label-danger');
							} else {
								el.removeClass('label-danger').addClass('label-info');
							}
						}
					});
				},

				/**
				 * Переключатель Важности новости
				 * @param id
				 * @param el
				 */
				toggleImportant = function (id, el) {
					post('/ajax/important', id, function (data) {
						if (data.status == 1) {
							el.text(data.title);
							if (data.title == 'важное') {
								el.removeClass('label-danger').addClass('label-info');
							} else {
								el.removeClass('label-info').addClass('label-danger');
							}
						}
					});
				},

				update = function (minutes) {
					minutes = minutes * 1000 * 60;
					if (minutes <= 0)
						return;

					if (getPageId() !== 1)
						return;

					setInterval(checkUpdate, minutes);
				},

				addNews = function (news) {
					var html = '<article data-key="' + news.id + '">' +
						'<div class="time" style="display: block;">' + app.helper.formatTime(news.date) + '</div>' +
						'<h2><a href="/' + news.id + '">' + news.title + '</a></h2>' +
						'<div class="desc"><a class="service" href="/go/' + news.link + '" target="_blank"><img src="https://www.google.com/s2/favicons?domain=' + news.service.web + '" alt=""></a> <span>' + (µ.utils.isEmpty(news.description) ? '' : news.description) + '</span></div>' +
						'<div class="foot"><span class="date">' + app.helper.formatDate(news.date) + '</span>' +
						(µ.utils.isEmpty(news.category) ? '' : ' | <a class="category" href="/' + news.category.tag + '">' + news.category.name + '</a>' ) +
						(µ.utils.isEmpty(news.html) ? '' : news.html ) +
						'</div></article>';

					$(html).insertBefore(getLastNewsInList());
				},

				checkUpdate = function () {
					$.getJSON('/ajax/list', {id: getLastNewsId()}, function (data) {
						if (!data.items)
							return;

						for (var newsDate in data.items) {

							if (!data.items.hasOwnProperty(newsDate))
								continue;

							addNews(data.items[newsDate]);
						}
					});
				};

			return {
				toggleHide: toggleHide,
				getNews: getNews,
				toggleImportant: toggleImportant,
				update: update
			}
		},
		post = function (url, id, success, error) {
			var errorFn = function (e) {
				console.error(e);
			};

			if (error == undefined)
				error = errorFn;

			$.post(url, {
				id: id
			})
				.success(success)
				.error(error);
		},

		cabinet = function () {
			var saveCategory = function (cb) {
				var id = cb.val(),
					value = cb[0].checked;

				$.post('/cabinet/ajax/save-category', {
					id: id,
					value: value
				})
					.success(function (data) {
						if (data.status != 1)
							return;
					});
			},
				saveService = function (cb) {
				var id = cb.val(),
					value = cb[0].checked;

				$.post('/cabinet/ajax/save-service', {
					id: id,
					value: value
				})
					.success(function (data) {
						if (data.status != 1)
							return;
					});
			};

			return {
				saveCategory: saveCategory,
				saveService: saveService
			}
		},


		helper = function () {
			var
				/**
				 * Форматирует Время
				 * @param time
				 * @returns {*}
				 */
				formatTime = function (time) {
					return moment.unix(time).format('HH:mm');
				},

				/**
				 * Форматирует Дату-Время
				 * @param time
				 * @returns {*}
				 */
				formatDateTime = function (time) {
					return moment.unix(time).format('D MMMM YYYY, HH:mm');
				},

				/**
				 * Форматирует Дату
				 * @param time
				 * @returns {*}
				 */
				formatDate = function (time) {
					return moment.unix(time).format('D MMMM YYYY');
				},

				parseCurrentURL = function () {
					var searchObject = {},
						queries, split, i;

					queries = location.search.replace(/^\?/, '').split('&');

					for (i = 0; i < queries.length; i++) {
						split = queries[i].split('=');
						searchObject[split[0]] = split[1];
					}

					return {
						protocol: location.protocol,
						host: location.host,
						hostname: location.hostname,
						port: location.port,
						pathname: location.pathname,
						search: location.search,
						searchObject: searchObject,
						hash: location.hash
					}
				};

			return {
				formatTime: formatTime,
				formatDateTime: formatDateTime,
				formatDate: formatDate,
				parseCurrentURL: parseCurrentURL
			}
		};

	appProto.prototype = {
		news: new news(),
		cabinet: new cabinet(),
		helper: new helper()
	};

	var app = new appProto();

	$('.news-view span.date').text(app.helper.formatDateTime($('.news-view span.date').text()));

	$.each($('.list-news article div.time'), function () {
		this.innerText = app.helper.formatTime(this.innerText);
		$(this).show();
	});

	$.each($('.list-news article .foot span.date'), function () {
		this.innerText = app.helper.formatDate(this.innerText);
	});

	$('.news-view .news-body a').attr('target', '_blank');

	$('.container')
		.on('click', 'article .news-hide', function (e) {
			e.preventDefault();
			var id = $(this).closest('article').data('key');
			app.news.toggleHide(id, $(this));
		})
		.on('click', 'article .news-important', function (e) {
			e.preventDefault();
			var id = $(this).closest('article').data('key');
			app.news.toggleImportant(id, $(this));
		});

	$('.cabinet-news-categories').on('click', 'label input:checkbox', function () {
		app.cabinet.saveCategory($(this));
	});

	$('.cabinet-news-services').on('click', 'label input:checkbox', function () {
		app.cabinet.saveService($(this));
	});

	$(document).on('click', '.share-social-item a', function (e) {
		e.preventDefault();
		var social = $(this).closest('.share-social-list'),
			title = social.data('title'),
			desc = social.data('desc'),
			img = social.data('img'),
			url = social.data('url')
			;
		if ($(this).hasClass('socicon-vkontakte'))
			Share.vkontakte(url, title, img, desc);
		else if ($(this).hasClass('socicon-facebook'))
			Share.facebook_dlg(url, title, img, desc);
	});

	app.news.update(5);
});

