module.exports = function(grunt) {
	"use strict";

	var dirs = grunt.file.readJSON('grunt/directories.json');

	require('time-grunt')(grunt);

	require('load-grunt-config')(grunt, {
		loadGruntTasks: {
			pattern: ['grunt-*']
		},
		init: true,
		config: {
			pkg: grunt.file.readJSON('package.json'),
			dirs: dirs,
			scripts: {
				front : grunt.file.readJSON(dirs.front_js + '/require.json'),
				back : grunt.file.readJSON(dirs.back_js + '/require.json')
			}
		}
	});
};