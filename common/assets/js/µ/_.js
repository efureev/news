var µ = µ || {};

(function() {
	var global = this;

	µ.global = global;

	µ.utils = {};
	µ.format = {};

	global.µ = µ;
})();