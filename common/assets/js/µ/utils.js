µ.utils = µ.utils || {};

µ.utils = (function () {

	var erase = function (array, index, removeCount) {
			array.splice(index, removeCount);
			return array;
		},
		m = {
			/**
			 * Возвращает `true` если значение определенно
			 * @param {Object} value The value to test.
			 * @return {Boolean}
			 */
			isDefined: function(value) {
				return typeof value !== 'undefined';
			},

			/**
			 * Возвращает `true` если значение NULL
			 * @param value
			 * @returns {boolean}
			 */
			isNull: function(value) {
				return value == null;
			},

			/**
			 * Возвращает true  если значение пустое, false если нет. Значение будет пусто при:
			 *
			 * - `null`
			 * - `undefined`
			 * - пустой массив (array)
			 * - пустая строка, если не `allowEmptyString` == `true`
			 *
			 * @param {Object} Значение для проверки
			 * @param {Boolean} [allowEmptyString=false] `true` для разрешения пустых строк
			 * @return {Boolean}
			 */
			isEmpty: function(value, allowEmptyString) {
				return (value == null) || (!allowEmptyString ? value === '' : false) || (this.isArray(value) && value.length === 0);
			},

			/**
			 * Возвращает `true` если значение JavaScript Array.
			 *
			 * @param {Object} target Цель для проверки.
			 * @return {Boolean}
			 * @method
			 */
			isArray: ('isArray' in Array) ? Array.isArray : function(value) {
				return toString.call(value) === '[object Array]';
			},

			/**
			 * Возвращает индекс элемента найденного элемента в массиве. Если не найден, вернет "-1"
			 *
			 * @param {Array} array The array to check.
			 * @param {Object} item The item to find.
			 * @param {Number} from (Optional) The index at which to begin the search.
			 * @return {Number} The index of item in the array (or -1 if it is not found).
			 */
			indexOf: 'indexOf' in Array.prototype ? function(array, item, from) {
				return Array.prototype.indexOf.call(array, item, from);
			} : function(array, item, from) {
				var i, length = array.length;

				for (i = (from < 0) ? Math.max(0, length + from) : from || 0; i < length; i++) {
					if (array[i] === item) {
						return i;
					}
				}

				return -1;
			},

			/**
			 * Возвращает новый массив с уникальными значениями
			 *
			 * @param {Array} array
			 * @return {Array} results
			 */
			array_unique: function(array) {
				var clone = [],
					i = 0,
					ln = array.length,
					item;

				for (; i < ln; i++) {
					item = array[i];

					if (this.indexOf(clone, item) === -1) {
						clone.push(item);
					}
				}

				return clone;
			},

			/**
			 * Делает из строки массив (аналог PHP.explode)
			 *
			 * @param delim
			 * @param str
			 * @param lvl
			 * @returns {Array}
			 */
			explode : function (delim, str, lvl)
			{
				var out=[], tmp, pos;
				if (lvl)
				{
					tmp = str;
					pos = str.indexOf(d)
					while(lvl-1 && pos>=0)
					{
						out.push(tmp.substr(0, pos));
						tmp = tmp.substr(pos+delim.length);
						lvl--;
						pos = tmp.indexOf(delim);
					}
					out.push(tmp);
				}
				else
					out = str.split(delim);
				return out;
			},

			/**
			 * Фильтрует массив и удаляет пустые элементы, как определяет {@link µ.utils.isEmpty}.
			 *
			 * @param {Array} array
			 * @return {Array} results
			 */
			array_clean: function(array) {
				var results = [],
					i = 0,
					ln = array.length,
					item;

				for (; i < ln; i++) {
					item = array[i];

					if (!this.isEmpty(item)) {
						results.push(item);
					}
				}

				return results;
			},

			array_last : function (array)
			{
				array.pop();
			},

			array_first : function (array)
			{
				array.shift();
			},

			/**
			 * Удаляет элемент из массива если существует
			 *
			 * @param {Array} array
			 * @param {Object} item
			 * @return {Array}
			 */
			array_remove: function(array, item) {
				var index = this.indexOf(array, item);

				if (index !== -1) {
					erase(array, index, 1);
				}

				return array;
			}

	};

	return m;
})();