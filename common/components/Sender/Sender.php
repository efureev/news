<?php

namespace common\components\Sender;

use common\models\News;
use console\components\Help;
use GuzzleHttp\Client;
use yii\base\Component;
use yii\base\InvalidParamException;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * Class Sender
 * @package common\components\Sender
 *
 * @property string $url
 */
class Sender extends Component
{
	const TO_TELEGRAM = 1;

	public $to = self::TO_TELEGRAM;


	/**
	 * Отправляет новости реципиенту
	 *
	 * @param News $news
	 * @return bool
	 */
	public function send(News $news)
	{
		if (!app()->params['telegram']['send'])
			 return false;

		$msg = $news->getFrontendUrl();

		$bodyMsg = [
			'chat_id' => '@' . app()->params['telegram']['channel'],
			'text' => $msg
		];

		$client = new Client([
			'timeout' => 4.0,
			'json' => $bodyMsg,
			'headers' => [
				'Content-Type' => 'application/json'
			]
		]);

		$response = $client->post($this->url);

		$json = $response->getBody()->getContents();

		try {
			$responseArray = Json::decode($json);
			if ($responseArray['ok'] == true) {
				Help::successMsg('Ушло в Telegram..');
				return true;
			} else {
				Help::errorMsg('Ошибка отправлки в Telegram!');
				return false;
			}
		} catch (InvalidParamException $e) {
			Help::errorMsg('Ошибка отправлки в Telegram!');
			return false;
		}
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		$url = '';

		if ($this->to === self::TO_TELEGRAM) {
			$url = 'https://api.telegram.org/bot' . app()->params['telegram']['token'] . '/sendMessage';
		}

		return $url;
	}

}
