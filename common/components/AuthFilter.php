<?php

namespace common\components;

use Yii;
use yii\base\ActionFilter;
use yii\web\UnauthorizedHttpException;

class AuthFilter extends ActionFilter {

	public function beforeAction($action) {
		if (app()->user->can('moderator'))
			return true;
		throw new UnauthorizedHttpException('Нет прав на доступ');
	}

}
