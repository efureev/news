<?php

namespace common\components\NewsService\services;
use common\components\NewsService\NewsService;
use common\models\NewsResources;
use common\models\NewsServiceCategory;

/**
 * Класс для работы с внешними сервисами новостей
 */

class Service_uralweb extends NewsService
{
	protected function normalizeLink ($link){
		return trim($link);
	}

	protected function normalizeUniqueId ($item){
		return preg_replace('/http:\/\/www\.uralweb\.ru\/news\/(?:[\w]+)\/([\d]+).html/','$1',$item['link']);
	}

	protected function normalizeDescription ($desc){
		if (is_array($desc))
			return null;
		return trim($desc);
	}

	/**
	 * @param integer $newsId
	 * @param array $item
	 *
	 * @return NewsResources|null
	 */
	protected function savePreview ($newsId, $item){}

	protected function getCategory ($item){
		$catTag = preg_replace('/http:\/\/www\.uralweb\.ru\/news\/([\w]+)\/(?:[\d]+).html/','$1',$item['link']);
		/** @var NewsServiceCategory $cat */
		if ($cat = NewsServiceCategory::find()->where([
			'serviceId' => $this->service->id,
			'tag' => $catTag
		])->one())
			return $cat->categoryId;
		return null;
	}

	protected static function getPartsOfPage() {
		$images = [];

		$newsContent = pq('body #article_body');
		$imageblock = pq('body #photoblock img');


		/** @var \DOMElement $a */
		foreach($imageblock as $a) {
			$image = pq($a)->attr('src');
			$image = 'http:'.$image;
			$images [] = preg_replace('/[?\d]+$/','',$image);
		}

		return [
			'images' => $images,
			'content' => self::normalizeLinks($newsContent->html()),
		];

	}

	protected static function normalizeLinks($text) {
		return preg_replace('/<a href="\/link\?http/m','<a href="http://news-burg.ru/go/http',$text);
	}

}
