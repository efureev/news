<?php

namespace common\components\NewsService\services;

use common\components\NewsService\NewsService;
use common\models\Category;
use common\models\NewsResources;

/**
 * Класс для работы с внешними сервисами новостей
 */
class Service_e1 extends NewsService
{
	protected function normalizeLink($link)
	{
		return trim($link);
	}

	protected function normalizeUniqueId($item)
	{
		return preg_replace('/http\:\/\/www\.e1\.ru\/news\/spool\/news_id-([\d]+)\.html/', '$1', $item['link']);
	}

	protected function normalizeDescription($desc)
	{
		$desc = preg_replace('/<img([\s\w\d"\\/.=]+)>/', '', $desc);
		return trim($desc);
	}

	protected function normalizeTitle($title)
	{
		$title = str_replace(['(Фото)', '(Видео)', '(Документы)'], '', $title);
		return trim($title);
	}

	/**
	 * @param integer $newsId
	 * @param array $item
	 *
	 * @return NewsResources|null
	 */
	protected function savePreview($newsId, $item)
	{
		if (preg_match('/<img src=\\"([\d\w_\/.]+)\\"/', $item['description'], $m) && isset($m[1])) {
			$img = preg_replace('/_70_50_/', '_600_400_', $m[1]);
			$img = $this->service->fullWeb . $img;

			if (!$preview = NewsResources::findByLink($img)) {
				$preview = new NewsResources([
					'type' => NewsResources::TYPE_PHOTO,
					'main' => 1,
					'newsId' => $newsId
				]);
			}
			$preview->link = $img;
			$preview->save();
		}
	}

	protected function getCategory($item)
	{
		return Category::UNKNOWN;
	}


	protected static function getPartsOfPage()
	{
		$images = [];
		$res = [];

		$newsContent = pq('body table td.news');

		$titleContent = $newsContent->find('table:eq(1)');

		if (static::isAdvert($titleContent))
			return [
				'isAdvert' => true
			];

		$textContent = $newsContent->find('#newscontent');
		$textContent->find('table:first')->remove();

		$aa = $textContent->find('table:last tr a');

		/** @var \DOMElement $a */
		foreach ($aa as $a)
			$images [] = pq($a)->attr('href');

		if (count($images))
			$res['images'] = $images;

		$videoBlock = $textContent->find('iframe');

		if ($videoBlock) {
			$res['video'] = $videoBlock->attr('src');
		}

//		$textContent->find('table:last')->remove();
		$textContent->find('figure:first')->remove();
		$textContent->find('p:last')->remove();
		$textContent->find('iframe')->remove();;

		$categoryContent = $newsContent->find('a.small_orange');

		foreach($textContent->find('table') as $table)
			pq($table)->removeAttr('style');

		foreach($textContent->find('table a') as $a)
			pq($a)->removeAttr('class')->removeAttr('rel');

		foreach($textContent->find('table a img') as $img)
			pq($img)->removeAttr('style')->removeAttr('border')->addClass('photo');

		$res['content'] = self::normalizeLinks($textContent->html());

		if (!empty($categoryContent))
			$res['category'] = $categoryContent->text();

		return $res;
	}

	protected static function normalizeLinks($text)
	{
		return preg_replace('/<a href="http:/m', '<a href="http://news-burg.ru/go/http:', $text);
	}

	/**
	 * Рекламная статья или нет
	 *
	 * @param $text
	 * @return bool
	 */
	protected static function isAdvert($text)
	{
		/** @var \DOMElement $a */
		foreach ($text->find('tr td') as $td) {
			$trimText = mb_strtolower(trim(preg_replace('/^\s+|s+$/u', '', pq($td)->text())), 'UTF-8');
			if (preg_match('/на правах рекламы/', $trimText))
				return true;
		}
		return false;
	}
}
