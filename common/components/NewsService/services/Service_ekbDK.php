<?php

namespace common\components\NewsService\services;

use common\components\NewsService\NewsService;
use common\models\Category;
use common\models\NewsResources;


/**
 * Класс для работы с внешними сервисами новостей
 */

class Service_ekbDK extends NewsService
{

	/**
	 * @param integer $newsId
	 * @param array $item
	 *
	 * @return NewsResources|null
	 */
	protected function savePreview ($newsId, $item){}

	/**
	 * @param $item
	 *
	 * @return bool|mixed
	 */
	protected function normalizeUniqueId ($item){
		$pattern = '/^(?:.*?)-([\d]+)$/';
		if (preg_match($pattern,$item))
			return preg_replace($pattern,'$1',$item);
		return false;
	}

	protected function normalizeLink($link)
	{
		return preg_replace('/^(.*?)\?{1}(?:.*?)$/mi','$1',$link);
	}

	protected static function normalizeText($text)
	{
		return str_ireplace(['<div>', '</div>'], ['<p>', '</p>'], $text);
	}

	protected function getCategory ($item){
		return Category::UNKNOWN;
	}

	protected static function getPartsOfPage() {

		$newsContent = pq('body .base-content .col-1-3');

		$title = $newsContent->find('h1:first')->text();

		$newsOne = $newsContent->find('.news-one');

		$datetime = $newsOne->find('.noi-date')->text();
		$datetime = strtotime($datetime);

		$desc = $newsOne->find('.noc-lead')->text();

		$category = $newsOne->find('.noi-rubric')->text();
		$category = mb_strtolower($category,'UTF-8');

		$preview = $newsOne->find('.no-content .noc-photo img')->attr('src');
		$preview = 'http://www.dk.ru'.$preview;

		foreach($newsOne->find('.cke-content span') as $span)
			pq($span)->removeAttr('style');

		$text = $newsOne->find('.cke-content')->html();

		$text = self::normalizeLinks($text);
		$text = self::normalizeText($text);

		return [
			'title' => $title,
			'description' => $desc,
			'date' => $datetime,
			'category' => $category,
			'images' => [$preview],
			'content' => $text
		];

	}

	protected static function normalizeLinks($text) {
		$text = preg_replace('/href="http:\/\//m', 'href="http://news-burg.ru/go/http://', $text);
		return $text;
	}


}
