<?php

namespace common\components\NewsService\services;
use common\components\NewsService\NewsService;
use common\models\NewsResources;
use common\models\NewsServiceCategory;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Класс для работы с внешними сервисами новостей
 */

class Service_66ru extends NewsService
{
	protected function normalizeLink ($link){
		return trim($link);
	}

	protected function normalizeUniqueId ($item){
		return preg_replace('/66\.ru\:news\:([\d]+)/','$1',$item['guid']);
	}

	/**
	 * @param integer $newsId
	 * @param array $item
	 *
	 * @return NewsResources|null
	 */
	protected function savePreview ($newsId, $item){
		if (isset($item['enclosure']) && isset($item['enclosure']['@attributes']) && !empty($item['enclosure']['@attributes']['url'])) {

			if (!$preview = NewsResources::findByLink($item['enclosure']['@attributes']['url'])) {
				$preview =  new NewsResources([
					'type' => NewsResources::TYPE_PHOTO,
					'main' => 1,
					'newsId' => $newsId
				]);
			}

			$preview->link = $item['enclosure']['@attributes']['url'];
			$preview->save();
		}
	}

	protected function getCategory ($item){
		/** @var NewsServiceCategory $cat */
		if ($cat = NewsServiceCategory::find()->where([
			'serviceId' => $this->service->id,
			'name' => $item['category']
		])->one())
			return $cat->categoryId;
		return null;
	}


	protected static function getPartsOfPage() {

		$newsContent = pq('body .news-content');
		$videoblock = $newsContent->find('iframe');

		$res = [];

		if ($videoblock) {
			$videoblock->addClass('embed-responsive-item');
			$res['video'] = $videoblock->attr('src');
		}

		$res['images'] = self::parseGallery();

		$script = $newsContent->find('script');
		if ($script) {
			preg_match('/var data = \[(?P<name>[\d\D]*)\];/mU', $script, $match);

			if (isset($match['name'])) {
				preg_match_all("/img: '(?P<imgs>.*?)'/mU", $match['name'], $matchImg);

				if (isset($matchImg['imgs'])) {
					$res['images'] = ArrayHelper::merge($res['images'],$matchImg['imgs']);
				}
			}
		}
		$script->remove();

		foreach($newsContent->find('table a img') as $img)
			pq($img)->removeAttr('style');

		foreach($newsContent->find('table.simple-picture') as $table) {
			pq($table)->removeAttr('class');
		}

		$newsContent->find('.news-photoreportage-container')->remove();

		$res['content'] = self::normalizeLinks($newsContent->html());

		return $res;

	}

	protected static function parseGallery() {

		$photoreportage = pq('.news-photoreportage-container div');

		$images = [];

		/** @var \DOMElement $a */
		foreach($photoreportage as $a)
			$images [] = pq($a)->attr('data-href');

		return array_filter($images);
	}


	protected static function normalizeLinks($text) {
		$text = preg_replace('/http:\/\/66\.ru\/go\/http/m', 'http', $text);
		$text = preg_replace('/href="http:\/\//m', 'href="http://news-burg.ru/go/http://', $text);
		return $text;
	}

}
