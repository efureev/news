<?php

namespace common\components\NewsService;

use common\components\Harvester\Harvester;
use common\models\Category;
use common\models\News;
use common\models\NewsResources;
use common\models\NewsService as service;
use common\models\NewsServiceCategory;
use common\models\NotExistCategory;
use console\components\Exceptions\MissingRSSException;
use console\components\Help;
use GuzzleHttp\Client;
use yii\validators\UrlValidator;
use yii\web\BadRequestHttpException;

/**
 * Класс для работы с внешними сервисами новостей
 */
abstract class NewsService
{
	/**
	 * @var service|null
	 */
	protected $service;

	/**
	 * @var null|string
	 */
	private $rss;

	/** @var  array|null */
	private $rssArray;

	public $throwException = true;

	protected $_response;

	public static function run(service $service)
	{
		$class = 'common\\components\\NewsService\\services\\Service_' . $service->tag;

		/** @var NewsService $agregator */
		$agregator = new $class($service);

		if ($service->emailHarvest)
			$agregator->harvest();
		else
			$agregator->loadRSS()->processRssData();

		Help::successMsg('Успешно!');
	}

	public function __construct(service $service)
	{
		$this->service = $service;
		Help::warnMsg('Запуск сервиса новостей: ' . $service->name);
	}

	private function loadRSS()
	{
		if (empty($this->service->rss))
			throw new MissingRSSException();

		$this->rssArray = $this->parse($this->service->rss);

		Help::msg('Загрузили RSS файл');
		return $this;
	}

	private function parse($rss, $asArray = true)
	{
		libxml_use_internal_errors(true);

		$result = simplexml_load_file($rss, 'SimpleXMLElement', LIBXML_NOCDATA);

		if ($result === false) {
			Help::errorMsg('Ошибка загрузки RSS файла: ' . $this->service->rss);

			$errors = libxml_get_errors();
			$latestError = array_pop($errors);
			$error = [
				'message' => $latestError->message,
				'type' => $latestError->level,
				'code' => $latestError->code,
				'file' => $latestError->file,
				'line' => $latestError->line,
			];
			if ($this->throwException) {
				throw new BadRequestHttpException($latestError->message);
			}
			return $error;
		}

		if (!$asArray) {
			return $result;
		}

		return json_decode(json_encode($result), true);
	}

	private function processRssData()
	{
		$new = 0;
		foreach ($this->rssArray['channel']['item'] as $item) {
			$uniqueId = $this->normalizeUniqueId($item);

			$link = $this->normalizeLink($item['link']);
			$hash = News::getHashFromLink($link);

			$model = News::find()->where([
				'hash' => $hash
			]);

			$title = $this->normalizeTitle($item['title']);

			$categoryId = $this->getCategory($item);

			if (!$categoryId) {
				(new NotExistCategory([
					'name' => $item['category'],
					'serviceId' => $this->service->id
				]))->save();
				Help::errorMsg($this->service->name . ': не могу определить категорию: ' . $item['category']);
				continue;
			}

			$pubDate = $this->normalizeDate($item['pubDate']);
			$description = $this->normalizeDescription($item['description']);

			if (!$news = $model->one()) {
				$news = new News([
					'link' => $link,
					'guid' => $uniqueId,
					'serviceId' => $this->service->id,
				]);
				$new++;
			}

			$news->title = $title;

			if (empty($news->categoryId) || $news->categoryId === Category::UNKNOWN)
				$news->categoryId = $categoryId;

			$news->regionId = $this->service->regionId;
			$news->date = $pubDate;
			$news->description = $description;

			if ($news->save()) {
				$this->savePreview($news->id, $item);
			}
		}
		if ($new > 0) {
			Help::msg('Добавлено новых: ' . $new);
		}
	}


	protected function normalizeLink($link)
	{
		return trim($link);
	}

	protected function normalizeTitle($title)
	{
		return trim($title);
	}

	protected function normalizeDescription($desc)
	{
		return trim($desc);
	}

	protected function normalizeUniqueId($uniqId)
	{
		$uniqId = trim($uniqId);
		return empty($uniqId) ? null : $uniqId;
	}

	protected function normalizeDate($date)
	{
		return strtotime($date);
	}

	abstract protected function getCategory($category);

	abstract protected function savePreview($newsId, $item);


	public static function getFullPage(News $news)
	{
		$service = $news->service;

		if ($service->fullload === null)
			return false;

		$link = str_ireplace(['{web}', '{newsId}', '{guid}', '{similar_news_link}'], [$service->web, $news->id, $news->guid, $news->link], $service->fullload);

		if (!(new UrlValidator())->validate($link))
			return false;

		try {
			$content = self::getHtmlPage($news);
		} catch (\Exception $e) {
			return false;
		}


		if (!$service->isUtfCharset) {
		    try {
		        $content = iconv($service->charset, 'UTF-8//IGNORE', $content);
            } catch (\yii\base\ErrorException $e) {
                echo $e->getMessage().PHP_EOL;
            }
        }

		\phpQuery::newDocumentHTML($content);


		/** @var NewsService $class */
		$class = 'common\\components\\NewsService\\services\\Service_' . $service->tag;

		$data = $class::getPartsOfPage();

		if (!empty($data['isAdvert'])) {
			Help::warnMsg($service->name . ': #' . $news->id . ' рекламная статья');
			$news->text = '[Реклама]';
			$news->visible = 0;
			return $news->update();
		}


		$data['content'] = self::trim($data['content']);

		$data['content'] = self::setAbsoluteLinks($data['content'], $service->fullWeb);

		$news->text = $data['content'];

		if (isset($data['title'])) {
			$news->title = $data['title'];
		}

		if (isset($data['description'])) {
			$news->description = $data['description'];
		}

		if (!empty($data['date'])) {
			$news->date = $data['date'];
		}

		if (!empty($data['category'])) {
			$data['category'] = mb_strtolower($data['category'], 'UTF-8');

			/** @var NewsServiceCategory $cat */
			if ($cat = NewsServiceCategory::find()->where([
				'serviceId' => $service->id,
				'name' => $data['category']
			])->one()
			) {
				$news->categoryId = $cat->categoryId;
			} else {
				/** @var NewsServiceCategory $findedCategory */
				if ($findedCategory = NewsServiceCategory::searchSimilar($data['category'])) {
					$category = NewsServiceCategory::addCategory($findedCategory, $service);
					$news->categoryId = $category->id;
				} else {
					l('Не известная категория новостей «' . $data['category'] . '» в сервисе «' . $service->name . '»');

					(new NotExistCategory([
						'name' => $data['category'],
						'serviceId' => $service->id
					]))->save();
				}
			}
		}

		if (isset($data['video'])) {

			if (!$video = NewsResources::findByLink($link, $news->id)) {
				$video = new NewsResources([
					'type' => NewsResources::TYPE_VIDEO,
					'newsId' => $news->id,
					'link' => $data['video']
				]);
				$video->save();
			}
		}

		if (empty($news->description)) {
			$news->setDescriptionText();
		}

		$news->visible = 1;


		if ($news->update()) {
			if (isset($data['images'])) {
				$hasPreview = $news->preview ? true : false;

				foreach ($data['images'] as $img) {
					$link = preg_match('/https?:\/\//', $img) ? $img : $service->fullWeb . $img;
					if (!$preview = NewsResources::findByLink($link)) {
						$preview = new NewsResources([
							'type' => NewsResources::TYPE_PHOTO,
							'newsId' => $news->id,
						]);

						$preview->link = $link;

						if (!$hasPreview)
							$hasPreview = $preview->main = 1;

						$preview->save();
					}
				}
			}
			Help::successMsg($service->name . ': #' . $news->id . ' ок');
			app()->sender->send($news);
		} else {
			Help::errorMsg($service->name . ': #' . $news->id . ' fail');
			cdump($news->errors,3,0);
			echo PHP_EOL;
			cdump($news->attributes,3,0);
			echo PHP_EOL;
			cdump('link: '.$news->link,3,0);
			echo PHP_EOL;
			cdump('#id: '.$news->id);
			echo PHP_EOL;
		}

	}

	protected static function getPartsOfPage()
	{
		return [];
	}

	protected static function normalizeLinks($text)
	{
		return $text;
	}

	public static function trim($text)
	{
		$text = preg_replace('/ {2,}/', '$1', $text);
		$text = preg_replace('/<p>(\s*)<\/p>/mu', '', $text);
		$text = preg_replace('/^(\s+)|(\s+$)/mu', '', $text);
		return $text;
	}

	public static function setAbsoluteLinks($text, $prefix)
	{
		$text = preg_replace('/<a href="\//', '<a href="' . $prefix . '/', $text);
		$text = preg_replace('/ src="\//', ' src="' . $prefix . '/', $text);
		return $text;
	}

	public static function deleteLinks($text)
	{
		return preg_replace('/<a(?:.*?)>(.*?)<\/a>/m', '$1', $text);
	}


	protected static function getHtmlPage(News $news)
	{
		return (new Client(['timeout' => 6.0]))->get($news->link)->getBody()->getContents();
	}

	/**
	 * Сбор урлов с писем и вставка пустых "новостей"
	 *
	 * @return bool
	 */
	public function harvest()
	{
		try {
			$urls = Harvester::getUrlsFromEmail();
		} catch (\Exception $e) {
			Help::errorMsg('Ошибка получения почты..');
			Help::errorMsg($e->getMessage());
			return false;
		}

		if (!$urls)
			return false;

        $i = 0;

        foreach ($urls as $url) {
			$url = $this->normalizeLink($url);
			if (($uniqId = $this->normalizeUniqueId($url)) === false)
				continue;

			$hash = News::getHashFromLink($url);

			$model = News::find()->where([
				'hash' => $hash
			]);

			$categoryId = $this->getCategory($url);

			if (!$categoryId) {
				Help::errorMsg($this->service->name . ': не могу определить категорию: ');
				continue;
			}

			if (!$news = $model->one()) {
				$news = new News([
					'link' => $url,
					'title' => '[empty]',
					'date' => time(),
					'regionId' => $this->service->regionId,
					'guid' => $uniqId,
					'visible' => 0,
					'serviceId' => $this->service->id,
					'categoryId' => $categoryId
				]);
				if ($news->save()) {
					$i++;
				}
			}
		}
		if ($i == 0) {
			Help::msg('Нет ни одной новой новости. Удаляем письмо');
			Harvester::removeEmail();
		}

		if ($i > 0) {
			Help::msg('Добавлено новых: ' . $i);
		}
		return true;
	}

	public static function clear()
	{
		Help::msg('Запуск очистки');
	}

}
