<?php

namespace common\components\authclient;

class Google extends \yii\authclient\clients\Google {

    public $host = 'https://google.com';

    protected function defaultNormalizeUserAttributeMap() {
        $map = parent::defaultNormalizeUserAttributeMap();

		$map ['email'] = function ($info) {
			if (!isset($info['emails'][0]['value']))
				return null;
			return $info['emails'][0]['value'];
		};

		$map ['sex'] = function ($info) {
			if ($info['gender'] == 'male')
				return 'M';
			if ($info['gender'] == 'female')
				return 'F';
			return null;
		};

		$map ['uid'] = 'id';

		$map ['lastname'] = function ($info) {
			if (!isset($info['name']['familyName']))
				return null;
			return $info['name']['familyName'];
		};

		$map ['firstname'] = function ($info) {
			if (!isset($info['name']['givenName']))
				return null;
			return $info['name']['givenName'];
		};

		$map ['network'] = function () {
			return $this->name;
		};

		$map ['short_pathname'] = function ($info) {
			return preg_replace('/https:\/\/plus\.google\.com\//','',$info['url']);
		};

		$map ['photo'] = function ($info) {
			if (!isset($info['image']['url']))
				return null;
			return preg_replace('/\?sz=50$/','?sz=200',$info['image']['url']);
		};

        return $map;
    }
}
