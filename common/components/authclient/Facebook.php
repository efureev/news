<?php

namespace common\components\authclient;

class Facebook extends \yii\authclient\clients\Facebook {

    public $host = 'https://www.facebook.com';

    protected function initUserAttributes() {
        $attributes = $this->api('me', 'GET', [
            'fields' => implode(',', [
                    'birthday',
                    'email',
                    'first_name',
                    'last_name',
                    'gender',
                    'id',
                    'link',
                    'timezone',
                    'picture',
                    'name'
                ]),
        ]);

        return $attributes;
    }

    protected function defaultNormalizeUserAttributeMap() {
        $map = parent::defaultNormalizeUserAttributeMap();
        $map ['firstname'] = 'first_name';
        $map ['lastname'] = 'last_name';
        $map ['uid'] = 'id';
        $map ['email'] = 'email';
        $map ['short_pathname'] = 'id';


        $map ['dob'] = function ($info) {
            if (!isset($info['birthday']))
                return '0000-00-00';
            return date('Y-m-d', strtotime($info['birthday']));
        };

        $map ['network'] = function () {
            return $this->name;
        };

        $map ['photo'] = function ($info) {
            if (!isset($info['picture']))
                return null;
            return $info['picture']['data']['url'];
        };

        $map ['sex'] = function ($info) {
            if (!isset($info['gender']))
                return null;
            if ($info['gender'] == 'male')
                return 'M';
            if ($info['gender'] == 'female')
                return 'F';
            return null;
        };
        return $map;
    }
}
