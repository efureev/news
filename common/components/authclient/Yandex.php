<?php

namespace common\components\authclient;

class Yandex extends \yii\authclient\clients\Yandex {

    public $host = 'http://yandex.ru';

	protected function initUserAttributes() {
		$attributes = $this->api('info', 'GET');

	//	dump($attributes);

		return $attributes;
	}

    protected function defaultNormalizeUserAttributeMap() {
        $map = parent::defaultNormalizeUserAttributeMap();

		$map ['lastname'] = function ($info) {
			if (!isset($info['last_name']))
				return null;
			return $info['last_name'];
		};

		$map ['firstname'] = function ($info) {
			if (!isset($info['first_name']))
				return null;
			return $info['first_name'];
		};

		$map ['email'] = function ($info) {
			if (!isset($info['default_email']))
				return null;
			return $info['default_email'];
		};

		$map ['sex'] = function ($info) {
			if ($info['sex'] == 'male')
				return 'M';
			if ($info['sex'] == 'female')
				return 'F';
			return null;
		};

		$map ['uid'] = 'id';

		$map ['network'] = function () {
			return $this->name;
		};

		$map ['dob'] = function ($info) {
			if (!isset($info['birthday']))
				return '0000-00-00';
			return $info['birthday'];
		};

		$map ['short_pathname'] = function ($info) {
			return $info['login'];
		};

        return $map;
    }
}
