<?php

namespace common\components\authclient;

class VKontakte extends \yii\authclient\clients\VKontakte {

	public $authUrl = 'https://oauth.vk.com/authorize';

	public $host = 'https://vk.com';

	public $scope = [
		'email'
	];

	public $attributeNames = [
		'uid',
		'first_name',
		'last_name',
		'nickname',
		'screen_name',
		'sex',
		'email',
		'bdate',
		'city',
		'country',
		'timezone',
		'domain',
		'photo',
		'photo_big',
		'photo_max_orig',
		'rate',
		'contacts'
	];

    protected function defaultNormalizeUserAttributeMap() {
        $map = parent::defaultNormalizeUserAttributeMap();
        $map ['firstname'] = 'first_name';
        $map ['lastname'] = 'last_name';
        $map ['nickname'] = 'nickname';
        $map ['short_pathname'] = 'domain';
        $map ['email'] = 'email';
        $map ['photo'] = 'photo';

        $map ['dob'] = function ($info) {
            return date('Y-m-d',strtotime($info['bdate']));
        };

        $map ['network'] = function () {
            return $this->name;
        };
        $map ['sex'] = function ($info) {
            if ($info['sex'] == 2)
                return 'M';
            if ($info['sex'] == 1)
                return 'F';
            return null;
        };
        return $map;
    }


}
