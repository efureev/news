<?php

namespace common\components\Mail;

use yii\base\ErrorException;

/**
 * Класс для работы с получением почты
 */
class Pop3
{

	private $socket = null;

	private $url = null;
	private $port = null;
	private $login = null;
	private $pass = null;

	public function __construct($url, $port, $login, $pass)
	{
		$this->login = $login;
		$this->pass = $pass;
		$this->port = $port;
		$this->url = $url;

		$this->connect();
		$this->auth();
	}

	public function __destruct()
	{
		$this->quit();
	}

	public static function getLastEmail($url, $port, $login, $pass)
	{
		$pop3 = new self($url, $port, $login, $pass);
		return $pop3->retr(1);
	}

	/**
	 * Создание подключения
	 *
	 * @return bool
	 * @throws ErrorException
	 */
	private function connect()
	{
		if (!$this->socket = fsockopen($this->url, $this->port, $errno, $errstr, 10))
			throw new ErrorException ("Не удается подключиться к " . $this->url . ": [" . $errno . "] " . $errstr);

		$this->verify('Ошибка подключения');
	}

	/**
	 * Аутентификация
	 *
	 * @return bool
	 * @throws ErrorException
	 */
	private function auth()
	{
		fwrite($this->socket, "USER $this->login\r\n");
		fwrite($this->socket, "PASS $this->pass\r\n");
		$this->verify('Ошибка авторизации');
	}

	/**
	 * Возвращает кол-во писем в почтовом ящике и их общий размер в байтах
	 *
	 * @return array
	 * @throws ErrorException
	 */
	public function stat()
	{
		fwrite($this->socket, "STAT\r\n");

		$answer = $this->verify('Ошибка получения статистики');

		list($answer, $messages_count, $common_len) = explode(' ', $answer);

		return [
			'messages_count' => (int)$messages_count,
			'common_len' => (int)$common_len,
		];
	}

	/**
	 * Разрыв связи с сервером
	 *
	 * @return bool
	 * @throws ErrorException
	 */
	public function quit()
	{
		fwrite($this->socket, "QUIT\r\n");
		$this->verify('Ошибка разрыва связи');
	}

	/**
	 * определить размер заданного сообщения (если номер не задан – возвращает размер для каждого сообщения)
	 *
	 * @param null|int $n номер сообщения
	 *
	 * @return bool
	 * @throws ErrorException
	 */
	public function listing()
	{
		$messages = [];

		$stat = $this->stat();

		for ($len = $stat['messages_count'], $i = 1; $i <= $len; $i++) {
			fwrite($this->socket, "LIST " . $i . "\r\n");
			$answer = $this->verify('Ошибка определения размера сообщений');

			list($answer, $msg_id, $msg_len) = explode(' ', $answer);

			$messages[] = [
				'msg_id' => $msg_id,
				'msg_len' => $msg_len
			];
		}
		return $messages;
	}

	/**
	 * Удаление сообщения {$n}
	 *
	 * @param int $n Номер сообщения
	 *
	 * @return bool
	 * @throws ErrorException
	 */
	public function delete($n)
	{
		fwrite($this->socket, "DELE " . $n . "\r\n");
		$this->verify('Ошибка удаления сообщения ' . $n);
	}

	public function retr($n)
	{
		fwrite($this->socket, "RETR " . $n . "\r\n");

		$msg = '';
		$head = '';

		//чтение ответа
		while (false !== ($s = fgets($this->socket))) {
			//строка .\r\n данные закончились
			if ($s === ".\r\n")
				break;

			//строка \r\n, заголовки закончились
			if ($s == "\r\n" && !$msg)
				$msg = ' ';

			if (!$msg)
				$head .= $s;
			else
				$msg .= $s;
		}

		return [
			'headers' => $head,
			'message' => $msg,
		];
	}


	private function verify($msg)
	{
		$answer = fgets($this->socket);
		if (strpos($answer, '+OK') !== 0)
			throw new ErrorException ($msg);
		return $answer;
	}

}
