<?php

namespace common\components\Mail;


/**
 * Класс для работы с получением почты
 */


class MailClient
{

	/*
		host connect to server
		example: mail.example.com
	*/
	public $server;

	/*
		port connect to server
		example: 110, 993, 995
	*/
	public $port = 110;

	/*
		type connect to server
		example: pop3, pop3/ssl, pop3/ssl/novalidate-cert
	*/
	public $type = "pop3";

	public $count;

	public $msg;

	/*
		user login connect to server
	*/
	private $user;

	/*
		user password connect to server
	*/
	private $pass;


	private $box;


	public function __destruct()
	{
		$this->close();
	}


	public function user($user, $pass)
	{

		$this->user = $user;

		$this->pass = $pass;

	}

	public function server($server, $port, $type)
	{

		$this->server = $server;

		$this->port = $port;

		$this->type = $type;

	}


	public function open()
	{


		$this->box = @imap_open("{" . $this->server . ":" . $this->port . "/" . $this->type . "}INBOX", $this->user, $this->pass);

		if ($this->box)
			return true;
		if (imap_last_error())
			$this->error(imap_last_error());
		else
			$this->error("Couldn't open stream  " . $this->server . ":" . $this->port . "...");

		return true;

	}

	/**
	 * Возвращает список почтовых ящиков в аккаунте
	 *
	 * @return array
	 */
	public function list_box()
	{
		$check = imap_getmailboxes($this->box, '{imap.yandex.ru}', '');
		return $check;
	}

	/**
	 * Статус аккаунта
	 *
	 * @return array
	 */
	public function stat()
	{
		$check = imap_mailboxmsginfo($this->box);
		return ((array)$check);
	}

	/**
	 * Выбрать письмо с id = {$id}
	 * @param $id
	 *
	 * @return object
	 */
	public function select($id)
	{
		$this->msg = $id;
		return imap_headerinfo($this->box, $id);
	}

	/**
	 * Количество писем
	 *
	 * @return int
	 */
	public function count()
	{
		$this->count = imap_num_msg($this->box);
		return (int)$this->count;
	}

	/**
	 * Получение тела сообщения
	 *
	 * @return string
	 */
	public function msg_body()
	{
		return imap_body($this->box, $this->msg);
	}

	/**
	 * Удаляет сообщение с id = {$id}
	 * @param $id
	 */
	public function delete($id)
	{
		imap_delete($this->box, $id);
		imap_expunge($this->box);
	}

	public function error($error)
	{
		echo $error;
		exit();
	}

	public function close()
	{
		if ($this->box)
			imap_close($this->box);
	}

}