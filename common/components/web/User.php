<?php

namespace common\components\web;

/**
 * Class User
 *
 * @package common\components\web
 *
 * @property \common\models\User $identity
 */
class User extends \yii\web\User {

    public function getFullName() {
        $name = [];
        if (!empty($this->identity->lastname))
            $name[] = $this->identity->lastname;
        if (!empty($this->identity->firstname))
            $name[] = $this->identity->firstname;
        if (empty($name))
            $name[] = $this->identity->nickname;
        return implode(' ',$name);
    }
}
