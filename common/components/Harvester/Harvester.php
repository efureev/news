<?php

namespace common\components\Harvester;

use common\components\Mail\MailClient;
use common\components\Mail\Pop3;


/**
 * Класс для работы с внешними сервисами новостей
 */
class Harvester
{

	/**
	 * Удаяем письмо
	 *
	 * @param int $n
	 * @return bool
	 */
	public static function removeEmail($n = 1)
	{
		$login = app()->params['yandex']['emails']['dk.harvest']['login'];
		$pass = app()->params['yandex']['emails']['dk.harvest']['pwd'];

		$box = new MailClient();
		$box->user($login,$pass);
		$box->server("imap.yandex.ru","993","imap/ssl/novalidate-cert");

		if ($box->open() && $box->count()) {
			$box->delete($n);
			return true;
		}
		return false;
	}


	/**
	 * Получаем все урлы из письма
	 *
	 * @param int $limit
	 *
	 * @return array
	 */
	public static function getUrlsFromEmail($limit = 50)
	{
		$login = app()->params['yandex']['emails']['dk.harvest']['login'];
		$pass = app()->params['yandex']['emails']['dk.harvest']['pwd'];


		$box = new MailClient();
		$box->user($login,$pass);
		$box->server("imap.yandex.ru","993","imap/ssl/novalidate-cert");
        $urls = [];
		if ($box->open() && $box->count()) {
			$select = $box->select(1);

			if (!self::isSendFromHost($select,'dk.ru'))
				return false;

			$body = $box->msg_body();

			$body = preg_replace('/=3D/', '=', $body);
			$body = preg_replace("%=\n|=\r\n|%mu", '', $body);


			if (preg_match_all('/<a href="(.*?)" style=/im', $body, $m) && count($m[1])) {
				foreach ($m[1] as $url) {
					if (count($urls) > $limit)
						break;
					$host = parse_url($url, PHP_URL_HOST);
					$path = parse_url($url, PHP_URL_PATH);

					if (!empty($host) && !empty($path) && strpos($host, 'us3.list-manage.com')===false && strpos($path, 'track/click')===false) {
//						$url = self::request($url);
//						$url = self::getLocationUrl($url);

						if (self::getIsNewsUrl($url))
							$urls[] = $url;
					}
				}
			}
		}

		return $urls;
	}

	/**
	 * Удостоверяемся, что то письмо с нужного хоста
	 * @param $mail
	 * @param $host
	 *
	 * @return bool
	 */
	private static function isSendFromHost($mail,$host)
	{
		if (!isset($mail->from) && count($mail->from))
			return false;

		foreach ($mail->from as $f) {
			if (isset($f->host) && $f->host == $host)
				return true;
		}
		return false;
	}

	/*private static function request($url)
	{
		$ch = curl_init();

		curl_setopt_array($ch, [
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_TIMEOUT => 2,
			CURLOPT_NOBODY => true,
			CURLOPT_HEADER => true
		]);

		$rawResponse = curl_exec($ch);
		curl_close($ch);

		return $rawResponse;
	}

	private static function getLocationUrl($header)
	{
		if (preg_match('/Location: (.*?)\n/im', $header, $m) && !empty($m[1])) {
			return trim($m[1]);
		}
		return null;
	}*/

	private static function getIsNewsUrl($url)
	{
		$path = parse_url($url, PHP_URL_PATH);
		return !empty($path) && preg_match('/^\/news\//', $path);
	}

}
