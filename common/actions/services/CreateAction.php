<?php

namespace common\actions\services;

use common\models\NewsService;

/**
 * Class CreateAction
 * @package common\actions\services
 *
 * Создание
 */
class CreateAction extends BaseAction
{

	/**
	 * @return mixed
	 */
    public function run()
    {
		$model = new NewsService();

		if ($model->load(app()->request->post()) && $model->save()) {
			return $this->controller->redirect(['@backend/views/services/view', 'id' => $model->id]);
		} else {
			return $this->controller->render('@backend/views/services/create', [
				'model' => $model,
			]);
		}
    }


}
