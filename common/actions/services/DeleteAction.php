<?php

namespace common\actions\services;

/**
 * Class DeleteAction
 * @package common\actions\services
 *
 */
class DeleteAction extends BaseAction
{

	/**
	 * @param $id
	 * @return mixed
	 * @throws \Exception
	 * @throws \yii\web\NotFoundHttpException
	 */
    public function run($id)
    {
		$this->findModel($id)->delete();

		return $this->controller->redirect(['services/index']);
    }

}
