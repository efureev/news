<?php

namespace common\actions\services;

/**
 * Class ViewAction
 * @package common\actions\services
 *
 */
class ViewAction extends BaseAction
{
	/**
	 * @param $id
	 * @return mixed
	 * @throws \yii\web\NotFoundHttpException
	 */
    public function run($id)
    {
		return $this->controller->render('@backend/views/services/view', [
			'model' => $this->findModel($id),
		]);
    }

}
