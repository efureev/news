<?php

namespace common\actions\services;

use common\models\NewsService;
use yii\base\Action;
use Yii;
use yii\web\NotFoundHttpException;


/**
 * Class BaseAction
 * @package common\actions\services
 *
 * Lists all News models.
 */
class BaseAction extends Action
{

	/**
	 * @param $id
	 * @return null|NewsService
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id)
	{
		if (($model = NewsService::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}


}
