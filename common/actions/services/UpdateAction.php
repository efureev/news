<?php

namespace common\actions\services;

/**
 * Class UpdateAction
 * @package common\actions\services
 *
 *
 */
class UpdateAction extends BaseAction
{
	/**
	 * @param $id
	 * @return mixed
	 * @throws \yii\web\NotFoundHttpException
	 */
    public function run($id)
    {
		$model = $this->findModel($id);

		if ($model->load(app()->request->post()) && $model->save()) {
			return $this->controller->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->controller->render('@backend/views/services/update', [
				'model' => $model,
			]);
		}
    }

}
