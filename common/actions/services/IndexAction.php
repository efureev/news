<?php

namespace common\actions\services;

use common\models\NewsService;
use yii\data\ActiveDataProvider;

/**
 * Class IndexAction
 * @package common\actions\services
 *
 * Создание новости
 */
class IndexAction extends BaseAction
{

    /**
     * Runs the action.
	 *
	 * @return string|\yii\web\Response
	 */
    public function run()
    {
		$dataProvider = new ActiveDataProvider([
			'query' => NewsService::find()
		]);

		return $this->controller->render('@backend/views/services/index', [
			'dataProvider' => $dataProvider,
		]);
    }

}
