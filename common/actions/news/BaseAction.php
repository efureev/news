<?php

namespace common\actions\news;

use common\models\News;
use yii\base\Action;
use Yii;
use yii\web\NotFoundHttpException;


/**
 * Class BaseAction
 * @package common\actions\news
 *
 * Lists all News models.
 */
class BaseAction extends Action
{

	/**
	 * @param $id
	 * @return null|News
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id)
	{
		if (($model = News::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}


}
