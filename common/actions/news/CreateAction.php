<?php

namespace common\actions\news;

use common\models\News;
use Yii;

/**
 * Class CreateAction
 * @package common\actions\news
 *
 * Создание новости
 */
class CreateAction extends BaseAction
{

    /**
     * Runs the action.
	 *
	 * @return string|\yii\web\Response
	 */
    public function run()
    {
		$model = new News();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->controller->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->controller->render('@backend/views/news/create', [
				'model' => $model,
			]);
		}
    }


}
