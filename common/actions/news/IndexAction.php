<?php

namespace common\actions\news;

use common\models\News;
use yii\base\Action;
use yii\data\ActiveDataProvider;
use Yii;


/**
 * Class IndexAction
 * @package common\actions\news
 *
 * Lists all News models.
 */
class IndexAction extends Action
{
	
    /**
     * Runs the action.
	 *
	 * @return string
	 */
    public function run()
    {
		$dataProvider = new ActiveDataProvider([
			'query' => News::find()->orderBy('date DESC')
		]);

		return $this->controller->render('@backend/views/news/index', [
			'dataProvider' => $dataProvider,
		]);
    }


}
