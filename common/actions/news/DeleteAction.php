<?php

namespace common\actions\news;

use Yii;

/**
 * Class DeleteAction
 * @package common\actions\news
 *
 * Срывает новость
 */
class DeleteAction extends BaseAction
{

    /**
     * Runs the action.
	 *
	 * @param $id
	 * @return \yii\web\Response
	 * @throws \yii\web\NotFoundHttpException
	 */
    public function run($id)
    {
		$this->findModel($id)->toggleVisible();
		return $this->controller->redirect(['index']);
    }


}
