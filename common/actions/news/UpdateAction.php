<?php

namespace common\actions\news;

use Yii;

/**
 * Class UpdateAction
 * @package common\actions\news
 *
 * Редактирование новости
 */
class UpdateAction extends BaseAction
{

    /**
     * Runs the action.
	 *
	 * @param $id
	 * @return string|\yii\web\Response
	 * @throws \yii\web\NotFoundHttpException
	 */
    public function run($id)
    {
		$model = $this->findModel($id);

		if ($model->load(app()->request->post()) && $model->save()) {
			return $this->controller->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->controller->render('@backend/views/news/update', [
				'model' => $model,
			]);
		}
    }


}
