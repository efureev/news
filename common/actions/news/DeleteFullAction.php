<?php

namespace common\actions\news;

use Yii;

/**
 * Class DeleteFullAction
 * @package common\actions\news
 *
 * Удаляет новость полностью и ее ресурсы
 */
class DeleteFullAction extends BaseAction
{
    /**
     * Runs the action.
	 *
	 * @param $id
	 * @return \yii\web\Response
	 * @throws \Exception
	 * @throws \yii\web\NotFoundHttpException
	 */
    public function run($id)
    {
		$this->findModel($id)->deleteResources()->delete();
		return $this->controller->redirect(['index']);
    }


}
