<?php

namespace common\actions\news;

use Yii;

/**
 * Class ViewAction
 * @package common\actions\news
 *
 * Просмотр одной новости
 */
class ViewAction extends BaseAction
{

    /**
     * Runs the action.
	 *
	 * @param $id
	 * @return string
	 * @throws \yii\web\NotFoundHttpException
	 */
    public function run($id)
    {
		return $this->controller->render('@backend/views/news/view', [
			'model' => $this->findModel($id),
		]);
    }


}
