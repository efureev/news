<?php

namespace common\actions\users;

use common\models\User;
use yii\data\ActiveDataProvider;
use Yii;


/**
 * Class IndexAction
 * @package common\actions\news
 *
 * Lists all News models.
 */
class IndexAction extends BaseAction
{
//	public $view;
    /**
     * Runs the action.
	 *
	 * @return string
	 */
    public function run()
    {
		$dataProvider = new ActiveDataProvider([
			'query' => User::find()->orderBy('nickname DESC')
		]);

		return $this->controller->render('@backend/views/users/index', [
			'dataProvider' => $dataProvider,
		]);
    }


}
