<?php

namespace common\actions\users;

use common\models\User;
use yii\base\Action;
use yii\web\NotFoundHttpException;



/**
 * Class BaseAction
 * @package common\actions\news
 *
 * Lists all News models.
 */
class BaseAction extends Action
{

	/**
	 * @param $id
	 * @return null|User
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id)
	{
		if (($model = User::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}


}
