<?php

namespace common\actions\crosscategory;

use common\models\NewsServiceCategory;
use yii\base\Action;
use Yii;
use yii\web\NotFoundHttpException;


/**
 * Class BaseAction
 * @package common\actions\services
 *
 * Lists all News models.
 */
class BaseAction extends Action
{

	/**
	 * @param $id
	 * @return null|NewsServiceCategory
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id)
	{
		if (($model = NewsServiceCategory::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}


}
