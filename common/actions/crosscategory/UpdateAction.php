<?php

namespace common\actions\crosscategory;

/**
 * Class UpdateAction
 * @package common\actions\crosscategory
 *
 *
 */
class UpdateAction extends BaseAction
{
	/**
	 * @param $id
	 * @return mixed
	 * @throws \yii\web\NotFoundHttpException
	 */
    public function run($id)
    {
		$model = $this->findModel($id);

		if ($model->load(app()->request->post()) && $model->save()) {
			return $this->controller->redirect(['crosscategory/index']);
		} else {
			return $this->controller->render('@backend/views/crosscategory/update', [
				'model' => $model,
			]);
		}
    }

}
