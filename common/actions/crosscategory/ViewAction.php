<?php

namespace common\actions\crosscategory;

/**
 * Class ViewAction
 * @package common\actions\crosscategory
 *
 */
class ViewAction extends BaseAction
{
	/**
	 * @param $id
	 * @return mixed
	 * @throws \yii\web\NotFoundHttpException
	 */
    public function run($id)
    {
		return $this->controller->render('@backend/views/crosscategory/view', [
			'model' => $this->findModel($id),
		]);
    }

}
