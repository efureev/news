<?php

namespace common\actions\crosscategory;

use common\models\NewsServiceCategory;

/**
 * Class CreateAction
 * @package common\actions\crosscategory
 *
 * Создание
 */
class CreateAction extends BaseAction
{

	/**
	 * @return mixed
	 */
    public function run()
    {
		$model = new NewsServiceCategory();

		if ($model->load(app()->request->post()) && $model->save()) {
			return $this->controller->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->controller->render('@backend/views/crosscategory/create', [
				'model' => $model,
			]);
		}
    }


}
