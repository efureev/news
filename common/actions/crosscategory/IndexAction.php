<?php

namespace common\actions\crosscategory;

use common\models\NewsServiceCategory;
use common\models\NewsServiceCategorySearch;
use yii\data\ActiveDataProvider;

/**
 * Class IndexAction
 * @package common\actions\crosscategory
 *
 */
class IndexAction extends BaseAction
{

    /**
     * Runs the action.
	 *
	 * @return string|\yii\web\Response
	 */
    public function run()
    {

		$searchModel = new NewsServiceCategorySearch();

		$dataProvider = $searchModel->search(app()->request->queryParams);

		return $this->controller->render('@backend/views/crosscategory/index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
    }

}
