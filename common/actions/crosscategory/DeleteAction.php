<?php

namespace common\actions\crosscategory;

/**
 * Class DeleteAction
 * @package common\actions\crosscategory
 *
 */
class DeleteAction extends BaseAction
{

	/**
	 * @param $id
	 * @return mixed
	 * @throws \Exception
	 * @throws \yii\web\NotFoundHttpException
	 */
    public function run($id)
    {
		$this->findModel($id)->delete();

		return $this->controller->redirect(['crosscategory/index']);
    }

}
