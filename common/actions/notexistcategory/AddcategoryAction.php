<?php

namespace common\actions\notexistcategory;
use common\models\Category;

/**
 * Class AddcategoryAction
 * @package common\actions\notexistcategory
 *
 */
class AddcategoryAction extends BaseAction
{

	/**
	 * @param $id
	 * @return mixed
	 * @throws \Exception
	 * @throws \yii\web\NotFoundHttpException
	 */
    public function run($id)
    {
		$model = $this->findModel($id);

		$category = new Category([
			'name' => $model->name
		]);

		if ($category->load(app()->request->post()) && $category->save()) {
			$model->delete();
			return $this->controller->redirect(['index']);
		} else {
			return $this->controller->render('@backend/views/category/create', [
				'model' => $category,
			]);
		}




	}

}
