<?php

namespace common\actions\notexistcategory;

use common\models\NewsServiceCategory;
use common\models\NewsServiceCategorySearch;
use common\models\NotExistCategory;
use yii\data\ActiveDataProvider;

/**
 * Class IndexAction
 * @package common\actions\notexistcategory
 *
 */
class IndexAction extends BaseAction
{

    /**
     * Runs the action.
	 *
	 * @return string|\yii\web\Response
	 */
    public function run()
    {

		$dataProvider = new ActiveDataProvider([
			'query' => NotExistCategory::find()
		]);

		return $this->controller->render('@backend/views/notexistcategory/index', [
			'dataProvider' => $dataProvider,
		]);
    }

}
