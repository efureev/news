<?php

namespace common\actions\notexistcategory;

/**
 * Class DeleteAction
 * @package common\actions\notexistcategory
 *
 */
class DeleteAction extends BaseAction
{

	/**
	 * @param $id
	 * @return mixed
	 * @throws \Exception
	 * @throws \yii\web\NotFoundHttpException
	 */
    public function run($id)
    {
		$this->findModel($id)->delete();

		return $this->controller->redirect(['notexistcategory/index']);
    }

}
