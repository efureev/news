<?php

namespace common\actions\notexistcategory;

use common\models\NewsServiceCategory;
use common\models\NotExistCategory;
use yii\base\Action;
use Yii;
use yii\web\NotFoundHttpException;


/**
 * Class BaseAction
 * @package common\actions\notexistcategory
 *
 * Lists all News models.
 */
class BaseAction extends Action
{

	/**
	 * @param $id
	 * @return null|NewsServiceCategory
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id)
	{
		if (($model = NotExistCategory::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}


}
