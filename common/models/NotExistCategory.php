<?php
namespace common\models;

use Yii;
use \yii\db\ActiveRecord;


/**
 * NotExistCategory model
 *
 * @property integer $id
 * @property string $name
 */
class NotExistCategory extends ActiveRecord
{


	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'notExistCategory';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'string'],
			[['name', 'serviceId'], 'required'],
			[['serviceId'], 'integer'],
			[['name','serviceId'], 'unique', 'targetAttribute' => ['name','serviceId']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID Категория',
			'name' => 'Название категории',
			'serviceId' => 'id сервиса'
		];
	}

	public function getService()
	{
		return $this->hasOne(NewsService::className(), ['id' => 'serviceId']);
	}
}