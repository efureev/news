<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;


/**
 * Category model
 *
 * @property integer $id
 * @property string $name
 * @property string $tag
 */
class Category extends ActiveRecord
{

	const STATUS_ENABLE = 1;
	const VISIBLE_IN_LIST = 1;

	/**
	 * Категория "НЕИЗВЕСНАЯ"
	 */
	const UNKNOWN = 100;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%categories}}';
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'tag'], 'string'],
			[['name', 'tag'], 'required'],
			[['enable', 'visibleInList'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID Категория',
			'name' => 'Название категории',
			'tag' => 'Тэг категории',
			'enable' => 'Включенная категория',
			'visibleInList' => 'Показывать в списке категорий'
		];
	}

	/**
	 * Возращает модель Неизвестной категории
	 * @return Category
	 */
	public static function getUnknownCategory()
	{
		return new self([
			'id' => 100,
			'name' => 'неизвестная',
			'tag' => 'unknown',
			'enable' => 1,
			'visibleInList' => 1
		]);
	}

	public static function find()
	{
		return new CategoryQuery(get_called_class());
	}

	/**
	 * Возвращает включенные категории для пользователя
	 *
	 * @return array|null
	 */
	public static function getEnableForUser()
	{
		if (app()->user->isGuest)
			return null;
		$categoriesEnable = app()->user->identity->newsConf;
		return isset($categoriesEnable['categories']) ? array_keys($categoriesEnable['categories']) : null;
	}

	/**
	 * Является ли категория Неизвестной
	 * @param integer|string $id
	 * @return bool
	 */
	public static function isUnknown($id)
	{
		return (int)$id === self::UNKNOWN;
	}

	public function beforeSave($insert)
	{
		$this->name = mb_strtolower($this->name, 'UTF-8');
		$this->tag = mb_strtolower($this->tag, 'UTF-8');
		return parent::beforeSave($insert);
	}
}

use \yii\db\ActiveQuery;

class CategoryQuery extends ActiveQuery
{

	public function enable()
	{
		return $this->andWhere(['enable' => Category::STATUS_ENABLE]);
	}

	public function inList()
	{
		return $this->andWhere(['visibleInList' => Category::VISIBLE_IN_LIST]);
	}

}
