<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;


/**
 * UserSocial model
 *
 * @property integer $id
 * @property string $username
 * @property string fullName
 * @property \common\models\User user
 */
class UserSocial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_social}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [
            [ [ 'uid', 'firstname', 'lastname', 'userId', 'nickname', 'dob', 'photo', 'network', 'email', 'phone', 'short_pathname' ], 'safe' ],
        ];
    }

    public function getUser() {
        return $this->hasOne('common\models\User', ['id' => 'userId']);
        // Первый параметр – это у нас имя класса, с которым мы настраиваем связь.
        // Во втором параметре в виде массива задаётся имя удалённого PK ключа  (id) и FK из текущей таблицы модели (author_id), которые связываются между собой
    }

    public function getFullName() {
        return $this->lastname . ' ' . $this->firstname;
    }


	/**
	 * @param $email
	 * @return null|static
	 */
	public static function findByEmail($email)
	{
		return static::findOne(['email' => $email]);
	}




}
