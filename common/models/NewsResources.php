<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;


/**
 * NewsResources model
 *
 * @property integer $id
 * @property string $title
 * @property bool $visible
 * @property bool $main
 * @property bool $isMain
 * @property bool $isPhoto
 * @property bool $isVideo
 * @property string $link
 * @property integer $newsId
 * @property string $type
 */
class NewsResources extends \yii\db\ActiveRecord
{

	const 	TYPE_PHOTO = 'photo',
			TYPE_VIDEO = 'video';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_resources}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function rules()
    {
        return [
			[['title','link'], 'string'],
			[['link'], 'required'],
			[['visible','main'], 'boolean'],
			[['newsId'], 'integer'],
			[['link'], 'url'],
			[['hash'], 'unique'],
			[['type'], 'safe'],
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'title' => 'Название ресурса',
			'link' => 'Линк'
		];
	}

	public function beforeValidate()
	{
		$this->hash = self::getHashFromLink($this->link);
		return parent::beforeValidate();
	}

	public function getIsPhoto()
	{
		return $this->type === self::TYPE_PHOTO;
	}

	public function getIsVideo()
	{
		return $this->type === self::TYPE_VIDEO;
	}

	public function getIsMain()
	{
		return $this->main == true;
	}


	public static function find()
	{
		return new NewsResourcesQuery(get_called_class());
	}

	/**
	 * Создает хеш из ссылки
	 * @param $link
	 *
	 * @return string
	 */
	public static function getHashFromLink($link)
	{
		return md5($link);
	}

	public static function findByLink ($link, $newsId = null) {
		$hash = self::getHashFromLink($link);
		$res = self::find()->where(['hash'=>$hash]);

		if ($newsId)
			$res->andWhere(['newsId'=>$newsId]);

		return $res->one();
	}



}

class NewsResourcesQuery extends \yii\db\ActiveQuery
{

	public function enable()
	{
		return $this->andWhere(['visible' => 1]);
	}


}
