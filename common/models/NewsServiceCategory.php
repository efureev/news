<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;


/**
 * NewsServiceCategory model
 *
 * @property integer $id
 * @property string $name
 * @property string $tag
 * @property integer $categoryId
 * @property Category $category
 * @property integer $serviceId
 */
class NewsServiceCategory extends \yii\db\ActiveRecord
{

	const STATUS_ENABLE = 'Y';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%news_services_categories}}';
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'tag'], 'string'],
			[['name'], 'required'],
			[['categoryId', 'serviceId'], 'integer'],
			[['enable'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Название категории',
			'tag' => 'Тэг категории',
			'categoryId' => 'Соответствует катигории',
			'serviceId' => 'Сервис новостей',
			'enable' => 'Включенная категория',
		];
	}

	public function beforeSave($insert)
	{
		$this->name = mb_strtolower($this->name, 'UTF-8');
		if (!empty($this->tag))
			$this->tag = mb_strtolower($this->tag, 'UTF-8');

		return parent::beforeSave($insert);
	}

	public function getService()
	{
		return $this->hasOne(NewsService::className(), ['id' => 'serviceId']);
	}

	public function getCategory()
	{
		return $this->hasOne(Category::className(), ['id' => 'categoryId']);
	}

	public static function find()
	{
		return new NewsServiceCategoryQuery(get_called_class());
	}

	/**
	 * Найти похожую категорию
	 *
	 * @param $name
	 *
	 * @return null|Category
	 */
	public static function searchSimilar($name)
	{
		$findedCats = self::find()->where('name = :name', [':name' => $name])->all();

		$res = [];

		/** @var NewsServiceCategory $findedCat */
		foreach ($findedCats as $findedCat) {
			$res[$findedCat->category->id] = isset($res[$findedCat->category->id]) ? $res[$findedCat->category->id] + 1 : 1;
		}

		$catId = null;
		$max = 0;

		foreach ($res as $kCat => $vCat) {
			if ($vCat > $max) {
				$max = $vCat;
				$catId = $kCat;
			}
		}

		if ($catId !== null)
			return Category::findOne($catId);

		return null;
	}

	/**
	 * Добавляет новую категорию у сервиса
	 *
	 * @param Category $category
	 * @param NewsService $service
	 *
	 * @return bool|self
	 */
	public static function addCategory(Category $category, NewsService $service)
	{
		if ($model = self::findOne([
			'name' => $category->name,
			'serviceId' => $service->id
		])
		)
			return $model;

		$model = new self([
			'name' => $category->name,
			'categoryId' => $category->id,
			'serviceId' => $service->id
		]);

		if ($model->save()) {
			l('Добавлена новая категория новостей «' . $category->name . '» для сервиса «'.$service->title. '»');
			return $model;
		}
		return null;
	}

}

class NewsServiceCategoryQuery extends \yii\db\ActiveQuery
{

	public function enable()
	{
		return $this->andWhere(['enable' => NewsServiceCategory::STATUS_ENABLE]);
	}


}
