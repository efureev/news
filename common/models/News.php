<?php
namespace common\models;

use frontend\widgets\ModeratorMenu;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;
use \yii\db\ActiveRecord;


/**
 * News model
 *
 * @property integer $id
 * @property string $title
 * @property string $hash
 * @property string $link
 * @property boolean $important    важность статьи
 * @property integer $categoryId
 * @property integer $views            Количество просмотров
 * @property string $date
 * @property string $description
 * @property string $text
 * @property boolean $visible
 * @property string $guid
 * @property string $serviceIcon
 * @property integer $regionId
 *
 * @property NewsService $service
 * @property NewsResources $preview
 * @property NewsResources[] $resources
 * @property NewsResources[] $gallery     Теже ресурсы, только без preview
 * @property Category $category
 */
class News extends ActiveRecord
{

	const STATUS_ENABLE = 1;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%news}}';
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title', 'link'], 'string'],
			[['title', 'link', 'date', 'categoryId', 'regionId', 'serviceId'], 'required'],
			[['hash'], 'unique'],
			[['visible', 'important'], 'boolean', 'on' => 'addFromWeb'],
			[['categoryId', 'regionId', 'date', 'serviceId'], 'integer'],
			[['link'], 'url'],
			[['text', 'guid', 'description'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'title' => 'Заголовок новости',
			'date' => 'Дата',
			'text' => 'Текст',
			'resources' => 'Ресурсы',
			'service.name' => 'Драйвер'
		];
	}

	public function beforeValidate()
	{
		$this->hash = self::getHashFromLink($this->link);
		return parent::beforeValidate();
	}

	/**
	 * @return NewsQuery
	 */
	public static function find()
	{
		return (new NewsQuery(get_called_class()))->with(['service', 'category', 'resources']);
	}

	public function getRegion()
	{
		return $this->hasOne(Region::className(), ['id' => 'regionId']);
	}

	public function getCategory()
	{
		return $this->hasOne(Category::className(), ['id' => 'categoryId']);
	}

	public function getService()
	{
		return $this->hasOne(NewsService::className(), ['id' => 'serviceId']);
	}

	public function getResources()
	{
		return $this->hasMany(NewsResources::className(), ['newsId' => 'id']);
	}

	public function getGallery()
	{
		return $this->hasMany(NewsResources::className(), ['newsId' => 'id'])->where(['main' => 0]);
	}

	public function getPreview()
	{
		return $this->hasOne(NewsResources::className(), ['newsId' => 'id'])->where(['main' => 1]);
	}

	/**
	 * Удаляем все ресурсы статьи
	 * @return $this
	 */
	public function deleteResources()
	{
		NewsResources::deleteAll('newsId = :newsId', [':newsId' => $this->id]);
		return $this;
	}

	/**
	 * Создает хеш из ссылки
	 *
	 * @param $link
	 *
	 * @return string
	 */
	public static function getHashFromLink($link)
	{
		return md5($link);
	}

	/**
	 * Возвращает иконку сервиса
	 *
	 * @return string
	 */
	public function getServiceIcon()
	{
		return Html::img('https://www.google.com/s2/favicons?domain=' . $this->service->web);
	}

	public function toggleVisible()
	{
		NewsResources::updateAll([
			'visible' => !$this->visible
		], 'newsId = :newsId', [':newsId' => $this->id]);
		$this->visible = !$this->visible;
		return $this->update();
	}

	public function toggleImportant()
	{
		$this->important = !$this->important;
		return $this->update();
	}

	/**
	 * Установка высокого приоритета
	 *
	 * @return bool|int
	 * @throws \Exception
	 */
	public function setImportant()
	{
		$this->important = 1;
		return $this->update();
	}

	/**
	 * Установка нормального приоритета
	 *
	 * @return bool|int
	 * @throws \Exception
	 */
	public function setNotImportant()
	{
		$this->important = 0;
		return $this->update();
	}

	/**
	 * Устанавливает краткое описание, если нет. По-умолчанию берет первый абзац из текста
	 *
	 * @param string|null $desc
	 *
	 * @return $this
	 */
	public function setDescriptionText($desc = null)
	{
		if ($desc !== null) {
			$this->description = $desc;
			return $this;
		}

		if (empty($this->text)) {
			l('#' . $this->id . ' > пустое поле TEXT', $inLogger = 'warning', $category = 'application');
			return $this;
		}
		$desc = $this->text;

		$desc = self::deleteLinks($desc);

		if (preg_match('/^<p>(.*?)<\/p>/', $desc, $m)) {
			$this->description = $m[1];
		} else {
			l('#' . $this->id . ' > DESC:' . $desc . ' > MATCH:' . print_r($m, 1), $inLogger = 'warning', $category = 'application');
		}

		return $this;
	}

	private static function deleteLinks($text)
	{
		return preg_replace('/<a(?:.*?)>(.*?)<\/a>/m', '$1', $text);
	}

	/**
	 * Возвращает url статьи на фронтенде
	 * @return string
	 */
	public function getFrontendUrl()
	{
		return Yii::getAlias('@web.frontend').'/'.$this->id;
	}

	public function getCategoryName()
	{
		if (Category::UNKNOWN == $this->categoryId) {
			return false;
		} else {
			if (!$this->category instanceof Category)
				return false;
		}
		return $this->category->name;
	}

	public static function listToArray($value)
	{
		if (!is_array($value))
			return [];

		$res = [];

		/** @var News $news */
		foreach ($value as $news) {
			$res[$news->date] = [
				'id' => $news->id,
				'title' => $news->title,
				'date' => $news->date,
				'description' => $news->description,
				'link' => $news->link,
				'service' => [
					'id' => $news->service->id,
					'name' => $news->service->name,
					'web' => $news->service->web,
				],
				'category' => Category::UNKNOWN == $news->categoryId || !$news->category instanceof Category
					? null
					: [
						'id' => $news->category->id,
						'name' => $news->category->name,
						'tag' => $news->category->tag,
					],
				'region' => !$news->service->region instanceof Region
					? null
					: [
						'id' => $news->service->region->id,
						'name' => $news->service->region->name,
					],
				'html' => ModeratorMenu::widget(['news' => $news,'prefix'=>' | adm: '])
			];
		}
		return $res;
	}

}

use \yii\db\ActiveQuery;

class NewsQuery extends ActiveQuery
{

	/**
	 * @return NewsQuery
	 */
	public function enable()
	{
		return $this->andWhere(['visible' => News::STATUS_ENABLE]);
	}

	public function allNews()
	{
		return $this;
	}

	/**
	 * Разрешает только определенные Категории
	 *
	 * @param $ids
	 * @return NewsQuery
	 */
	public function category($ids)
	{
		if (!is_array($ids))
			$ids = [$ids];
		return $this->andWhere(['categoryId' => $ids]);
	}

	/**
	 * Разрешает только определенные Сервисы
	 *
	 * @param $ids
	 * @return $this
	 */
	public function services($ids)
	{
		if (!is_array($ids))
			$ids = [$ids];
		return $this->andWhere(['serviceId' => $ids]);
	}

	/**
	 * @return NewsQuery
	 */
	public function emptyBody()
	{
		return $this->andWhere(['text' => null]);
	}

	public function loaded()
	{
		return $this->andWhere(['<>', 'text', 'null']);
	}

	public function toDateTime(\DateTime $dateTime)
	{
		return $this->andWhere(['<', 'date', $dateTime->getTimestamp()]);
	}

}
