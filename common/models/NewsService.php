<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;


/**
 * NewsService model
 *
 * @property integer $id
 * @property string $name
 * @property string $rss
 * @property string $tag
 * @property string $web
 * @property string $fullWeb
 * @property string $regionId
 * @property boolean $isWinCharset
 * @property boolean $isUtfCharset
 * @property boolean $emailHarvest	Собирает новости из почты
 *
 * @property Region region
 */
class NewsService extends \yii\db\ActiveRecord
{

	const STATUS_ENABLE = 1;

	public $paramsData = [];

	public $rss = null;

	/**
	 * @var null|string Адрес страницы для загрузки статьи
	 */
	public $fullload = null;

	public $charset = 'UTF-8';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_services}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function rules()
    {
        return [
			[['name','tag','web','rss','fullload','charset'], 'string'],
			[['name','tag','web','regionId'], 'required'],
			[['regionId'], 'integer'],
			[['emailHarvest'], 'boolean'],
			[['enable','params'], 'safe'],
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID Сервиса',
			'name' => 'Название сервиса новостей',
			'tag' => 'Тэг сервиса',
			'web' => 'Официальный сайт',
			'enable' => 'Включенный сервис',
			'rss' => 'RSS лента',
			'regionId' => 'Регион',
			'fullload' => 'Шаблон для загрузки статьи',
			'emailHarvest' => 'Собирать новости из Почты'
		];
	}

	public function beforeSave($insert)
	{
		if (empty($this->rss))
			unset($this->paramsData['rss']);
		else
			$this->paramsData['rss'] = $this->rss;

		if (empty($this->fullload))
			unset($this->paramsData['fullload']);
		else
			$this->paramsData['fullload'] = $this->fullload;

		if (empty($this->charset))
			unset($this->paramsData['fullloacharsetd']);
		else
			$this->paramsData['charset'] = $this->charset;

		if (!empty($this->paramsData))
			$this->params = json_encode($this->paramsData);

		return parent::beforeSave($insert);
	}

	public function afterFind()
	{
		$this->paramsData = json_decode($this->params,true);
		$this->rss = isset($this->paramsData['rss']) ? $this->paramsData['rss'] : null;
		$this->fullload = isset($this->paramsData['fullload']) ? $this->paramsData['fullload'] : null;
		$this->charset = isset($this->paramsData['charset']) ? $this->paramsData['charset'] : null;
		parent::afterFind();
	}

	public function getRegion()
	{
		return $this->hasOne(Region::className(), [ 'id' => 'regionId']);
	}


	public function getFullWeb()
	{
		return 'http://'.$this->web;
	}

	public function getIsUtfCharset()
	{
		return $this->charset == 'UTF-8';
	}

	public function getIsWinCharset()
	{
		return $this->charset == 'WINDOWS-1251';
	}

	public static function find()
	{
		return new NewsServiceQuery(get_called_class());
	}

	/**
	 * Разрешенные сервисы для пользователя
	 *
	 * @return array|null
	 */
	public static function getEnableForUser()
	{
		if (app()->user->isGuest)
			return null;
		$enable = app()->user->identity->newsConf;
		return isset($enable['services']) ? array_keys($enable['services']) : null;
	}
}

class NewsServiceQuery extends \yii\db\ActiveQuery
{

	public function enable()
	{
		return $this->andWhere(['enable' => NewsService::STATUS_ENABLE]);
	}

	public function email($harvest = true)
	{
		return $this->andWhere(['emailHarvest' => $harvest]);
	}


}
