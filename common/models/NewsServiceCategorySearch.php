<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;


/**
 * NewsServiceCategorySearch
 *
 */
class NewsServiceCategorySearch extends NewsServiceCategory
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['categoryId', 'serviceId'], 'integer'],
			[['name'], 'string'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = NewsServiceCategory::find()->with(['category','service']);

		$dataProvider = new ActiveDataProvider([
			'query' => $query
		]);

		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'categoryId' => $this->categoryId,
			'serviceId' => $this->serviceId
		]);

		$query->andWhere('name LIKE "%' . $this->name . '%" ');

		return $dataProvider;
	}
}
