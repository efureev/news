<?php

namespace common\models;

use Yii;

/**
 * Класс Region
 *
 * @property integer $id
 * @property string $name
 */
class Region extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
	 * @table 'regions'
     */
    public static function tableName()
    {
        return 'regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['name'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['name'], 'unique']
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Регион'
        ];
    }
}
