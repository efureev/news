<?php
return [
	'adminEmail' => 'admin@example.com',
	'supportEmail' => 'support@example.com',
	'user.passwordResetTokenExpire' => 3600,

	'frontendUrl' => 'http://news-burg.ru',
	'backendUrl' => 'http://back.news-burg.ru',

	'backend' => false,

	'social' => [
		'vk' => [
			'client_id' => '3247909',
			'client_secret' => '',
			'api_version' => '5.40',
			//'redirect_uri' => 'http://news-burg.ru/callback/vk',
			'groupId' => 40175928,
		]
	],

	'telegram' => [
		'send' => true,
		'channel' => 'NewsBurg'
	],

	'yandex' => [
		'emails' => [
			'dk.harvest' => [
				'login' => 'dk.harvest@news-burg.ru',
				'pwd' => ''
			]
		]
	],
];
