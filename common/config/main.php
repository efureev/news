<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'ru-RU',
    'sourceLanguage' => 'en-EN',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'harvester' => [
            'class' => 'common\components\Harvester\Harvester',
        ],
        'sender' => [
            'class' => 'common\components\Sender\Sender',
        ],
        'session' => [
            'class' => 'yii\redis\Session',
            'timeout' => 3600 * 24 * 7,
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
               /* 'google' => [
                    'class' => 'yii\authclient\clients\GoogleOpenId'
                ],*/
                'facebook' => [
                    'class' => 'common\components\authclient\Facebook',
                    'clientId' => '489825551032773',
                    'clientSecret' => 'd1a926a262c956d8f3ce86df44ffd3a7',
                ],

				'vkontakte' => [
					'class' => 'common\components\authclient\VKontakte',
					'clientId' => '3247909',
					'clientSecret' => 'tJASIMLPaeN6Nht1JLi2',
				],

				'google' => [
					'class' => 'common\components\authclient\Google',
					'clientId' => '95389479577-kpjebut2f1m7bv2tpi8322f16e3kimi4.apps.googleusercontent.com',
					'clientSecret' => 'hX6LWbhlJJM3OVVxM1psukmm'
				],

				'yandex' => [
					'class' => 'common\components\authclient\Yandex',
					'clientId' => '29c6eab368f7474a89d3d75e8ce309c7',
					'clientSecret' => 'f7f1adbe728b47d9ad69e3b894c913e2'
				],
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => false,
                'yii\jui\JuiAsset' => false,
                'yii\web\YiiAsset' => false,
                'yii\bootstrap\BootstrapAsset' => false,
                'yii\bootstrap\BootstrapPluginAsset' => false
            ],
        ],
    ],
];

