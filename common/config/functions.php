<?php

use yii\helpers\VarDumper;
/**
 * Алиас
 * @return BaseApplication|WebApplication|ConsoleApplication
 */
function app() {
	return \Yii::$app;
}

/**
 * Алиас для \Yii::$app->db
 * @return \yii\db\Connection
 */
function db() {
	return app()->db;
}

/**
 * Логгер
 * @param        $text
 * @param string $inLogger
 * @param string $category
 */
function l ($text, $inLogger = 'info', $category = 'application') {
	echo print_r($text,1).PHP_EOL;
	if ($inLogger) {
		if (!in_array($inLogger, ['info','error','warning','trace']))
			\Yii::info($text, $inLogger);
		else
			\Yii::$inLogger($text, $category);
	}
}


/**
 * Дамп для браузера
 * @param      $var
 * @param int  $lvl
 * @param bool $end
 */
function dump($var, $lvl=5, $end = true) {
	cdump($var, $lvl, $end, true);
}

/**
 * Дамп для консоли
 * @param      $var
 * @param int  $lvl
 * @param bool $end
 * @param bool $highlight
 */
function cdump($var, $lvl=5, $end = true, $highlight = false) {
	echo VarDumper::dumpAsString($var, $lvl, $highlight);
	if ($end)
		app()->end();
}

function isInt($value, $allowZero = true) {
	if (!$allowZero && $value===0)
		return false;
	return filter_var($value,FILTER_VALIDATE_INT);
}