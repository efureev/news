<?php
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('vendor', dirname(dirname(__DIR__)) . '/vendor');

Yii::setAlias('@web.frontend', 'https://news-burg.ru');
Yii::setAlias('@web.backend', 'https://back.news-burg.ru');
Yii::setAlias('@web.frontend.adm', 'https://news-burg.ru/adm');

require(__DIR__ . '/functions.php');
